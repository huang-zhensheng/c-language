#include"11_20.h"
namespace std
{
	void test_vector1()
	{
		std::vector<int>v;

		//v.reserve(8);//先增容

		v.push_back(1);
		v.push_back(2);
		v.push_back(3);
		v.push_back(4);
		std::vector<int>::iterator pos = find(v.begin(), v.end(), 2);
		if (pos != v.end())
		{
			pos = v.insert(pos, 20);//2的前面插入20
		}
		//在insert以后pos就失效了
		cout << *pos << endl;//访问
		*pos = 100;//修改
		pos++;
		for (auto e : v)
		{
			cout << e << " ";
		}
		cout << endl;
	}
}
namespace std
{
	void test_vector2()
	{
		std::vector<int>v;
		v.push_back(1);
		v.push_back(2);
		v.push_back(3);
		v.push_back(4);
		//要求删除v中所有偶数
		std::vector<int>::iterator it = v.begin();
		while (it != v.end())
		{
			if (*it % 2 == 0)
			{
				//erase返回删除数据的下一个数据的位置
				it = v.erase(it);
			}
			else
			{
				it++;
			}
		}
		for (auto e : v)
		{
			cout << e << " ";
		}
		cout << endl;
	}
}

int main()
{
	//hzs::test_vector1();
	//hzs::test_vector2();
	//hzs::test_vector3();
	//hzs::test_vector4();
	//hzs::test_vector5();
	//hzs::test_vector6();
	hzs::test_vector7();
	//std::test_vector1();
	//std::test_vector2();
	return 0;
}
