#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include<iostream>
using namespace std;
#include<vector>
#include<string>
#include<assert.h>
namespace hzs
{
	template<class T>
	class vector
	{
	public:
		typedef T*iterator;
		typedef const T*const_iterator;
		////v2(v1)
		////第一种传统写法
		//vector(const vector<T>& v)
		//{
		//	//开一块空间
		//	_start = new T[v.capacity()];
		//	//拷贝有效数据过去
		//	memcpy(_start, v._start, sizeof(T)*v.size());
		//	_finish = _start + v.size();
		//	_endofstorage = _start + v.capacity();
		//}

		////第二种传统写法
		//vector(const vector<T>& v)
		//	:_start(nullptr)
		//	, _finish(nullptr)
		//	, _endofstorage(nullptr)
		//{
		//	//一次性把空间给开好
		//	reserve(v.capacity());
		//	//遍历v1，把v1的数据插入到v2，插入push_back会自己开空间
		//	for (const auto& e : v)
		//	{
		//		push_back(e);
		//	}
		//}

		//v2(v1)
		//现代写法
		//类模板的成员函数，还可以在定义模板参数
		template<class InputIterator>
		vector(InputIterator first, InputIterator last)
			:_start(nullptr)
			, _finish(nullptr)
			, _endofstorage(nullptr)
		{
			while (first != last)//左闭右开[ )
			{
				push_back(*first);
				first++;
			}
		}
		vector(const vector<T>& v)
			:_start(nullptr)
			, _finish(nullptr)
			, _endofstorage(nullptr)
		{
			vector<T>tmp(v.begin(), v.end());
			this->swap(tmp);//v1和tmp交换
		}
		void swap(vector<T>& v)
		{
			std::swap(this->_start,v._start);
			std::swap(this->_finish,v._finish);
			std::swap(this->_endofstorage,v._endofstorage);
		}


		////v1 = v4
		////传统写法
		//vector<T>& operator = (const vector<T>& v)
		//{
		//	if (this ! = &v)
		//	{
		//		delete[] _start;//释放掉之前的空间
		//		_start = _finish = _endofstorage = nullptr;
		//		//开跟你v4一样大的空间
		//		reserve(v.capacity());
		//		//遍历v4,插入数据
		//		for (const auto& e : v)
		//		{
		//			push_back(e);
		//		}
		//		return *this;
		//	}
		//}

		//v1 = v4
		//现代写法
		vector<T>& operator = (vector<T> v)//直接使用传值
		{
			swap(v);
			return *this;
		}

		//析构函数
		~vector()
		{
			delete[] _start;
			_start = _finish = _endofstorage = nullptr;
		}
		size_t capacity()
		{
			return _endofstorage - _start;
		}
		size_t size()
		{
			return _finish - _start;
		}
		const_iterator begin() const
		{
			return _start;
		}
		const_iterator end() const
		{
			return _finish;
		}
		iterator begin() 
		{
			return _start;
		}
		iterator end() 
		{
			return _finish;
		}
		T&operator[](size_t i)
		{
			assert(i<size());
			return _start[i];
		}
		const T&operator[](size_t i) const
		{
			assert(i<size());
			return _start[i];
		}
		vector()
			:_start(nullptr)
			, _finish(nullptr)
			, _endofstorage(nullptr)
		{}
		//开空间
		void reserve(size_t n)
		{
			if (n > capacity())
			{
				size_t sz = this->size();
				//动态申请n个T类型的空间
				T*tmp = new T[n];
				//从_start位置开始向后复制sizeof(T)*size()字节的数据到tmp的内存位置
				memcpy(tmp, _start, sizeof(T)*size());
				_start = tmp;
				_finish = _start + sz;
				_endofstorage = _start + n;
			}
		}
		//开空间+初始化
		void resize(size_t n, const T& val = T())
		{
			if (n <= size())
			{
				_finish = _start + n;
			}
			else//n>size()
			{
				//空间容量不够
				if (n > capacity())
				{
					reserve(n);
				}
				//size()<n<capacity()
				while (_finish < _start + n)//指针
				{
					*_finish = val;
					_finish++;
				}
			}
		}
		//push_back
		void push_back(const T& x)
		{
			if (_finish == _endofstorage)
			{
				size_t newcapacity = capacity() == 0 ? 4 : capacity() * 2;
				reserve(newcapacity);
			}
			*_finish = x;
			_finish++;
		}
		//pop_back
		void pop_back()
		{
			assert(!empty());
			_finish--;
		}
		iterator/*返回值*/ insert(iterator pos, const T& x)
		{
			//判断pos的位置是不是合法的
			assert(pos >= _start && pos <= _finish);
			//记录一下pos的相对位置
			size_t len = pos - _start;
			//开空间
			if (_finish == _endofstorage)
			{
				//万一为空的时候为0，所以按下面的方式进行
				size_t newcapacity = capacity() == 0 ? 4 : capacity() * 2;

				//reserve之后里面的_start会更新
				reserve(newcapacity);
				//更新一下pos的位置
				pos = _start + len;
			}
			//挪动数据
			iterator end = _finish - 1;
			while (end >= pos)
			{
				*(end + 1) = *end;
				end--;
			}
			//插入数据
			*pos = x;
			_finish++;
			return pos;
		}
		iterator erase(iterator pos)
		{
			assert(pos >= _start && pos < _finish);
			iterator it = pos + 1;
			while (it != _finish)
			{
				*(it - 1) = *it;
				it++;
			}
			_finish--;
			return pos;
		}
	private:
		iterator _start;
		iterator _finish;
		iterator _endofstorage;
	};	
	void print(vector<int>&v)
	{
		//第一种遍历方式
		vector<int>::iterator it = v.begin();
		while (it != v.end())
		{
			cout << *it << " ";
			it++;
		}
		cout << endl;
		////第二种遍历方式
		//for (auto e : v)
		//{
		//	cout << e << " ";
		//}
		//cout << endl;
		////第三种遍历方式
		//for (size_t i = 0; i < v.size(); i++)
		//{
		//	cout << v[i] << " ";
		//}
		//cout << endl;
	}
	void test_vector1()
	{
		vector<int>v;
		v.push_back(1);
		v.push_back(2);
		v.push_back(3);
		v.push_back(4);
		v.push_back(5);
		//第一种遍历方式
		vector<int>::iterator it = v.begin();
		while (it != v.end())
		{
			cout << *it << " ";
			it++;
		}
		cout << endl;
		//第二种遍历方式
		for (auto e : v)
		{
			cout << e << " ";
		}
		cout << endl;
		//第三种遍历方式
		for (size_t i = 0; i < v.size(); i++)
		{
			cout << v[i] << " ";
		}
		cout << endl;
		print(v);
	}
	void test_vector2()
	{
		vector<int>v;
		v.push_back(1);
		v.push_back(2);
		v.push_back(3);
		v.push_back(4);
		v.push_back(5);
		vector<int>::iterator it = v.begin();
		while (it != v.end())
		{
			cout << *it << " ";
			it++;
		}
		cout << endl;
	}
	void test_vector3()
	{
		vector<int>v;
		v.push_back(1);
		v.push_back(2);
		v.push_back(3);
		v.push_back(4);
		v.push_back(5);
		print(v);

		v.resize(2);
		print(v);

		v.resize(4);
		print(v);

		v.resize(8,1);
		print(v);
	}
	void test_vector4()
	{
		vector<string>v;

		string s("helllo");
		v.push_back(s);
		v.push_back(string("hello"));//构造一个匿名对象去做实参
		v.push_back("hello");//隐式类型转换

		v.push_back("hello");
		v.push_back("hello");
		v.push_back("hello");
		for (auto e : v)
		{
			cout << e << " ";
		}
		cout << endl;
	}
	void test_vector5()
	{
		vector<int>v;

		//v.reserve(8);//先增容

		v.push_back(1);
		v.push_back(2);
		v.push_back(3);
		v.push_back(4);
		vector<int>::iterator pos = find(v.begin(), v.end(), 2);
		if (pos != v.end())
		{
			pos = v.insert(pos, 20);//2的前面插入20
		}
		//在insert以后pos就失效了
		cout << *pos << endl;//访问
		*pos = 100;//修改
		pos++;
		for (auto e : v)
		{
			cout << e << " ";
		}
		cout << endl;
	}
	void test_vector6()
	{
		vector<int>v;
		v.push_back(1);
		v.push_back(2);
		v.push_back(3);
		v.push_back(4);
		//要求删除v中所有偶数
		vector<int>::iterator it = v.begin();
		while (it != v.end())
		{
			if (*it % 2 == 0)
			{
				//erase返回删除数据的下一个数据的位置
				it = v.erase(it);
			}
			else
			{
				it++;
			}
		}
		for (auto e : v)
		{
			cout << e << " ";
		}
		cout << endl;
	}
	void test_vector7()
	{
		vector<int>v1;
		v1.push_back(1);
		v1.push_back(2);
		v1.push_back(3);
		v1.push_back(4);

		for (auto e : v1)
		{
			cout << e << " ";
		}
		cout << endl;
		//拷贝
		vector<int>v2(v1);
		for (auto e : v2)
		{
			cout << e << " ";
		}
		cout << endl;

		string s("hello");
		vector<int>v3(v1.begin(), v1.end());
		vector<int>v4(s.begin(), s.end());

		//赋值
		v1 = v4;
		for (auto e : v1)
		{
			cout << e << " ";
		}
		cout << endl;
	}
}