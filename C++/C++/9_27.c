#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//Definition for singly-linked list.
struct ListNode
{
	int val;
	struct ListNode *next;
};
struct ListNode* removeElements(struct ListNode* head, int val)
{
	if (head == NULL)
		return NULL;
	struct ListNode*cur = head;
	struct ListNode*newhead = NULL;
	struct ListNode*tail = NULL;
	while (cur)
	{
		struct ListNode*next = cur->next;
		if (cur->val == val)
		{
			free(cur);
		}
		else
		{
			if (tail == NULL)
			{
				newhead = tail = cur;

			}
			else
			{
				 cur = tail->next;
				 tail = cur;
			}
		}
		cur = next;
	}
	if (tail)
		tail->next = NULL;
	return newhead;
	//if (head == NULL)
	//	return NULL;
	//struct ListNode* newnode = NULL;
	//struct ListNode* tail = NULL;
	////取不是val的节点尾插
	//struct ListNode* cur = head;

	//while (cur)
	//{
	//	struct ListNode* next = cur->next;
	//	if (cur->val == val)
	//	{
	//		free(cur);
	//	}
	//	else
	//	{
	//		//尾插
	//		if (tail == NULL)
	//		{
	//			newnode = tail = cur;
	//		}
	//		else
	//		{
	//			tail->next = cur;
	//			tail = cur;
	//		}
	//	}
	//	cur = next;
	//	if (tail)
	//		tail->next = NULL;
	//}
	//return newnode;
}
int main()
{
	struct ListNode*n1 = (struct ListNode*)malloc(sizeof(struct ListNode));
	struct ListNode*n2 = (struct ListNode*)malloc(sizeof(struct ListNode));
	struct ListNode*n3 = (struct ListNode*)malloc(sizeof(struct ListNode));
	struct ListNode*n4 = (struct ListNode*)malloc(sizeof(struct ListNode));
	n1->val = 7;
	n2->val = 2;
	n3->val = 1;
	n4->val = 7;

	n1->next = n2;
	n2->next = n3;
	n3->next = n4;
	n4->next = NULL;
	struct ListNode* newnode = removeElements(n1, 7);
	while (newnode != NULL)
	{
		printf("%d->", newnode->val);
		newnode = newnode->next;
	}
	printf("NULL\n");
	return 0;
}
//void merge(int*nums1, int*nums2, int n, int m)
//{
//	int i1 = n-1;
//	int i2 = m-1;
//	int dest = m + n - 1;
//	while (i1 >=0 && i2 >= 0)
//	{
//		if (nums1[i1] > nums2[i2])
//		{
//			 nums1[dest] = nums1[i1];
//			 i1--;
//			 dest--;
//		}
//		else
//		{
//			 nums1[dest] = nums2[i2];
//			 i2--;
//			 dest--;
//		}
//	}
//	while (i2 >= 0)
//	{
//		 nums1[dest] = nums2[i2];
//		 dest--;
//		 i2--;
//	}
//}
//int main()
//{
//	int nums1[] = { 3, 6, 9, 0, 0, 0 };
//	int nums2[] = { 2, 5, 8 };
//	int n = 3;
//	int m = 3;
//	merge(nums1, nums2, n, m);
//	for (int i = 0; i < m + n ; i++)
//	{
//		printf("%d ",nums1[i]);
//	}
//	return 0;
//}

//int remove(int*arr,int NumSize,int val)
//{
//	int dest = 0;
//	int src = 1;
//	while (src < NumSize)
//	{
//		if (arr[src] == arr[dest])
//		{
//			src++;
//		}
//		else
//		{
//			dest++;
//			arr[dest] = arr[src];
//		}
//	}
//	return dest;
//}
//int main()
//{
//	int arr[] = { 1, 1, 2, 3, 3, 3 };
//	int NumSize = sizeof(arr) / sizeof(int);
//	int val = 3;
//	int ret = remove(arr,NumSize,val);
//	printf("%d\n",ret);
//	for (int i = 0; i < NumSize; i++)
//	{
//		printf("%d ",arr[i]);
//	}
//	return 0;
//}
