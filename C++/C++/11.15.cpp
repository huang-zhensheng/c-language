#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
#include<algorithm>
#include<functional>
using namespace std;
void test_vector1()
{
	vector<int>v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);
	//下标+[]
	for (size_t i = 0; i < v.size(); i++)
	{
		//v[i] -= 1;
		cout << v[i] << " ";
	}
	cout << endl;
	//迭代器
	vector<int>::iterator it = v.begin();
	while (it != v.end())
	{
		*it += 1;
		cout << *it << " ";
		it++;
	}
	cout << endl;
	//范围for
	for (auto e : v)
	{
		cout << e << " ";
	}
	cout << endl;

	vector<int>v1(v);
	for (auto e : v1)
	{
		cout << e << " ";
	}
	cout << endl;
}
void test_vector2()
{
	string s("hello world");
	vector<char>v(s.begin(), s.end());
	for (auto e : v)
	{
		cout << e << " ";
	}
	cout << endl;
}
void test_vector3()
{
	vector<int>v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);
	vector<int>::iterator it = v.begin();
	while (it != v.end())
	{
		cout << *it << " ";
		it++;
	}
	cout << endl;

	vector<int>::reverse_iterator rit = v.rbegin();
	while (rit != v.rend())
	{
		cout << *rit << " ";
		rit++;
	}
	cout << endl;
}
void test_vector4()
{
	vector<int>v;
	//cout << v.max_size() << endl;
	v.reserve(10);//开空间，改变容量
	//下面这样的用法是错误的——>operator[]中回去检查i是否小于size
	//for (size_t i = 0; i < 10; i++)
	//{
	//	v[i] = i;//assert(i<_size)
	//}
	for (size_t i = 0; i < 10; i++)
	{
		v.push_back(i);
	}
	v.resize(20, 1);//开空间+初始化
	for (size_t i = 0; i < 20; i++)
	{
		v[i] = i;
	}
}
void test_vector5()
{
	int a[] = { 1, 2, 3, 4, 5 };
	vector<int>v;
	//[first,last)
	v.assign(a, a + 4);
	for (auto e : v)
	{
		cout << e << " ";
	}
	cout << endl;
}
void test_vector6()
{
	int a[] = { 1, 2, 3, 4, 5 };
	vector<int>v(a, a + 5);
	//头插
	v.insert(v.begin(), 0);
	for (auto e : v)
	{
		cout << e << " ";
	}
	cout << endl;

	vector<int>::iterator pos = find(v.begin(), v.end(), 2);
	if (pos != v.end())
	{
		v.insert(pos, 20);
	}
	for (auto e : v)
	{
		cout << e << " ";
	}
	cout << endl;
	//默认排升序
	sort(v.begin(), v.end());
	for (auto e : v)
	{
		cout << e << " ";
	}
	cout << endl;
	//排降序—关于greater<int>是一个仿函数
	sort(v.begin(), v.end(), greater<int>());
	for (auto e : v)
	{
		cout << e << " ";
	}
	cout << endl;
}
void test_vector7()
{
	int a[] = { 0, 1, 2, 3, 4, 5 };
	vector<int>v(a, a + 5);
	for (auto e : v)
	{
		cout << e << " ";
	}
	cout << endl;
	//头删
	v.erase(v.begin());
	for (auto e : v)
	{
		cout << e << " ";
	}
	cout << endl;
	//删除pos位置的数
	vector<int>::iterator pos = find(v.begin(), v.end(), 2);
	if (pos != v.end())
	{
		v.erase(pos);
	}
	for (auto e : v)
	{
		cout << e << " ";
	}
	cout << endl;
}
int main()
{
	//test_vector1();
	//test_vector2();
	//test_vector3();
	//test_vector4();
	//test_vector5();
	//test_vector6();
	test_vector7();
	return 0;
}



//class Solution
//{
//public:
//	int singleNumber(vector<int>& nums)
//	{
//		int ret = 0;
//		for (size_t i = 0; i < nums.size(); i++)
//		{
//			ret = ret^nums[i];
//		}
//		return ret;
//
//		vector<int>::iterator it = nums.begin();
//		while (it != nums.end())
//		{
//			ret ^= (*it);
//			it++;
//		}
//		return ret;
//
//		for (auto e:nums)
//		{
//			ret += e;
//		}
//		return ret;
//	}
//};

