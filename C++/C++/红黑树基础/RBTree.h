#pragma once
enum Colour
{
	BLACK,
	RED
};
template<class K,class V>
struct RBTreeNode
{
	RBTreeNode<K, V>* _left;
	RBTreeNode<K, V>* _right;
	RBTreeNode<K, V>* _parent;

	Colour _col;
	pair<K, V> _kv;

	RBTreeNode(const pair<K, V>&kv)
		:_left(nullptr)
		, _right(nullptr)
		, _parent(nullptr)
		, _col(RED)
		, _kv(kv)
	{}
};
template<class K, class V>
class RBTree
{
	typedef RBTreeNode<K, V>Node;
public:
	RBTree()
		:_root(nullptr)
	{}
	bool Insert(const pair<K, V>& kv)
	{
		if (_root == nullptr)
		{
			_root = new Node(kv);
			_root->_col = BLACK;
			return true;
		}
		Node*parent = nullptr;
		Node*cur = _root;
		while (cur)
		{
			if (cur->_kv.first < kv.first)
			{
				parent = cur;
				cur = cur->_right;
			}
			else if (cur->_kv.first > kv.first)
			{
				parent = cur;
				cur = cur->_left;
			}
			else
			{
				return false;
			}
		}
		//新增结点，颜色是红色的，可能会产生连续的红色结点，不符合红黑树的规则
		cur = new Node(kv);
		cur->_col = RED;
		if (parent->_kv.first < kv.first)
		{
			parent->_right = cur;
			cur->_parent = parent;
		}
		else
		{
			parent->_left = cur;
			cur->_parent = parent;
		}
		//控制近似平衡
		while (parent && parent->_col == RED)
		{
			Node* grandfather = parent->_parent;
			if (parent == grandfather->_left)
			{
				Node*uncle = grandfather->_right;
				//情况一:uncle存在且为红，进行变色处理即可,并且继续往上更新处理
				if (uncle && uncle->_col == RED)
				{
					parent->_col = uncle->_col = BLACK;
					grandfather->_col = RED;
					//继续往上处理
					cur = grandfather;
					parent = cur->_parent;
				}
				//情况二+三:uncle不存在，或者存在且为黑，需要旋转+变色处理
				else
				{
					//情况二:单旋+变色
					if (cur == parent->_left)
					{
						RotateR(grandfather);
						parent->_col = BLACK;
						grandfather->_col = RED;
					}
					//情况三:双旋+变色
					else
					{
						RotateL(parent);
						RotateR(grandfather);
						cur->_col = BLACK;
						grandfather->_col = RED;
					}
					break;
				}
			}
			else //parent == grandfather->_right
			{
				Node*uncle = grandfather->_left;
				//情况一:uncle存在且为红，进行变色处理即可,并且继续往上更新处理
				if (uncle && uncle->_col == RED)
				{
					parent->_col = uncle->_col = BLACK;
					grandfather->_col = RED;
					//继续往上处理
					cur = grandfather;
					parent = cur->_parent;
				}
				//情况二+三:uncle不存在，或者存在且为黑，需要旋转+变色处理
				else
				{
					//情况二:单旋+变色
					if (parent->_right == cur)
					{
						RotateL(grandfather);
						parent->_col = BLACK;
						grandfather->_col = RED;
					}
					//情况三:双旋+变色
					else
					{
						RotateR(parent);
						RotateL(grandfather);
						cur->_col = BLACK;
						grandfather->_col = RED;
					}
					break;
				}
			}
		}
		_root->_col = BLACK;
		return true;
	}
	void RotateR(Node* parent)//右旋
	{
		Node* subL = parent->_left;
		Node* subLR = subL->_right;

		parent->_left = subLR;
		if (subLR)
			subLR->_parent = parent;

		Node*ppNode = parent->_parent;

		subL->_right = parent;
		parent->_parent = subL;

		if (parent == _root)
		{
			_root = subL;
			subL->_parent = nullptr;
		}
		else
		{
			if (ppNode->_left == parent)
				ppNode->_left = subL;
			else
				ppNode->_right = subL;

			subL->_parent = ppNode;
		}
	}
	void RotateL(Node* parent)//左旋
	{
		Node* subR = parent->_right;
		Node* subRL = subR->_left;

		parent->_right = subRL;

		if (subRL)
		{
			subRL->_parent = parent;
		}
		Node* ppNode = parent->_parent;

		subR->_left = parent;
		parent->_parent = subR;

		if (parent == _root)
		{
			_root = subR;
			subR->_parent = nullptr;
		}
		else
		{
			if (ppNode->_left == parent)
				ppNode->_left = subR;
			else
				ppNode->_right = subR;

			subR->_parent = ppNode;
		}
	}
	void _InOrder(Node* root)
	{
		if (root == nullptr)
		{
			return ;
		}
		else
		{
			_InOrder(root->_left);
			cout << root->_kv.first << " ";
			_InOrder(root->_right);
		}
	}
	void InOrder()
	{
		_InOrder(_root);
		cout << endl;
	}
	bool Check(Node* root)
	{
		if (root == nullptr)
		{
			return true;
		}
		if (root->_col == RED && root->_parent->_col == RED)
		{
			cout << "存在连续的红色节点" << endl;
			return false;
		}
		return Check(root->_left)
		&& Check(root->_right);
	}
	//检查每条路径黑色结点的数目
	bool Check_BLACK(Node* root, int blackNum, int benchmark)
	{
		if (root == nullptr)
		{
			if (blackNum != benchmark)
			{
				cout << "黑色结点的数目不相等" << endl;
				return false;
			}
			return true;
		}
		if (root->_col == BLACK)
		{
			blackNum++;
		}
		return Check_BLACK(root->_left, blackNum, benchmark)
			&& Check_BLACK(root->_right, blackNum, benchmark);
	}
	bool IsBalance()
	{
		if (_root == nullptr)
		{
			return true;
		}
		if (_root->_col == RED)
		{
			cout << "根节点是红色" << endl;
			return false;
		}
		//算出最左路径的黑色结点的数量作为基准值
		int benchmark = 0;
		Node* cur = _root;
		while (cur)
		{
			if (cur->_col == BLACK)
			{
				benchmark++;
			}
			cur = cur->_left;
		}
		int blackNum = 0;
		return Check(_root) && Check_BLACK(_root,blackNum,benchmark);
	}
private:
	Node* _root;
};
void TestRBTree()
{
	int a[] = { 16, 3, 7, 11, 9, 26, 18, 14, 15, 34, 100, 56 };
	RBTree<int, int>t1;
	for (auto e : a)
	{
		t1.Insert(make_pair(e, e));
	}
	cout << t1.IsBalance() << endl;
	t1.InOrder();
}