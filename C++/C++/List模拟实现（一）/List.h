#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<list>
#include<vector>
#include<algorithm>
#include<functional>
#include<time.h>
using namespace std;
#pragma once
namespace hzs
{
	template<class T>
	struct __list_node//内部使用
	{
		__list_node<T>* _next;
		__list_node<T>* _prev;
		T _data;

		__list_node(const T& x = T())
			:_next(nullptr)
			, _prev(nullptr)
			, _data(x)
		{}
	};
	// 迭代器(自定义类型)-- 用一个类去封装了节点的指针
	template<class T,class Ref,class Ptr>
	struct __list_iterator
	{
		typedef __list_iterator<T,Ref,Ptr> self;
		typedef __list_node<T> Node;
		Node* _node;//包含节点的指针

		__list_iterator(Node* node)
			:_node(node)//节点的指针初始化
		{}

		// *it->it.operator*()
		/*T& operator*()
		{
			return _node->_data;
		}*/
		Ref operator*()
		{
			return _node->_data;
		}

		// 前置++it
		self& operator++()
		{
			_node = _node->_next;

			return *this;
		}
		//后置it++  ->it.operator(0)
	    self operator++(int)//注意不能用引用返回
		{
			self tmp(*this);
			_node = _node->_next;
			return tmp;
		}
		//判断两个迭代器是否相等 —> 判断是否指向的是同一个节点
		bool operator!= (const self& it) const
		{
			return _node != it._node;
		}
	};

	template<class T>
	class list
	{
		typedef __list_node<T>  Node;
	public:
		typedef __list_iterator<T,T&,T*>  iterator;
		typedef __list_iterator<T,const T&,const T*>  const_iterator;

		//typedef __list_iterator<T>  const_iterator;
		iterator begin()
		{
			return iterator(_head->_next);//第一个位置的节点指针
		}

		iterator end()
		{
			return iterator(_head);//最后一个数据的下一个位置
		}
		const_iterator begin() const
		{
			return const_iterator(_head->_next);//第一个位置的节点指针
		}

		const_iterator end() const 
		{
			return const_iterator(_head);//最后一个数据的下一个位置
		}
		list()
		{
			// 带头双向循环
			//_head = new Node(T());
			_head = new Node;//头结点
			_head->_next = _head;
			_head->_prev = _head;
		}
		void push_back(const T& x)
		{
			Node* tail = _head->_prev;
			Node* newnode = new Node(x);
			//尾插
			tail->_next = newnode;
			newnode->_prev = tail;
			newnode->_next = _head;
			_head->_prev = newnode;
		}
		iterator insert(iterator pos, const T& x)
		{
			Node* cur = pos._node;
			Node* prev = cur->_prev;

			Node*newnode = new Node(x);
			prev->_next = newnode;
			newnode->_prev = prev;
			newnode->_next = cur;
			cur-> _prev = newnode;

			return  iterator(newnode);

			//或者这样写
			//iterator ret(newnode);
			//return ret;
		}
		//删除
		iterator erase(iterator pos)
		{
			asserr(pos != end());//不能删头结点

			Node*cur = pos._node;
			Node*prev = cur->_prev;
			Node*next = cur->_next;

			delete cur;
			prev->_next = next;
			next->_prev = prev;

			return iterator(next);
		}
		//删尾
		void pop_back()
		{
			erase(end()--);
		}
		//删头
		void pop_front()
		{
			erase(begin());
		}
		size_t size()
		{
			size_t n = 0;
			iterator it = begin();
			while (it != end())
			{
				it++;
				n++;
			}
			return n;
		}
		bool empty()
		{
			return begin() == end();
		}
	private:
		Node* _head;
	};
	void print_list(const list<int>& l)
	{
		list<int>::const_iterator it = l.begin();
		while (it != l.end())
		{
			cout << *it << " ";
			it++;
		}
		cout << endl;
	}

	void test_list1()
	{
		list<int> lt;
		lt.push_back(1);
		lt.push_back(2);
		lt.push_back(3);
		lt.push_back(4);
		print_list(lt);
		for (auto e : lt)
		{
			cout << e << " ";
		}
		cout << endl;

		list<int>::iterator it = lt.begin();
		while (it != lt.end())
		{
			//it.operator*() = 10;

			*it = 10;
			cout << *it << " ";
			++it;
		}
		cout << endl;

		for (auto e : lt)
		{
			cout << e << " ";
		}
		cout << endl;
	}
}