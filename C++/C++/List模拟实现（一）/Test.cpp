#include"List.h"
namespace std
{
	void test_list1()
	{
		list<int>lt1;
		list<int>lt2(10,5);
		list<int>lt3(lt2.begin(),lt2.end());

		vector<int> v = { 1, 2, 3, 4, 5 };
		list<int>lt4(v.begin(), v.end());
		list<int>::iterator it4 = lt4.begin();
		for (auto e : lt3)
		{
			cout << e << " ";
		}
		cout << endl;
		//这里用!= 因为不连续
		while (it4 != lt4.end())
		{
			cout << *it4 << " ";
			it4++;
		}
		cout << endl;

		lt3.assign(5, 3);
		for (auto e : lt3)
		{
			cout << e << " ";
		}
		cout << endl;
	}
	void test_list2()
	{
		list<int>lt;
		lt.push_back(1);
		lt.push_back(2);
		lt.push_back(3);
		lt.push_back(4);
		list<int>::iterator pos = find(lt.begin(), lt.end(), 2);
		if (pos != lt.end())
		{
			lt.insert(pos, 20);
		}
		//这里pos是否会失效呢？不会失效
		//1，vector insert->pos会失效，因为底层空间是物理连续数组，可能扩容，导致野指针问题
		//不扩容，挪动数据，也导致pos意义变了

		//2,list insert->pos不会失效，因为list是一个个独立节点，2前面插入数据是新增节点
		//pos还是指向2这个节点的，不会野指针，意义也没变
		cout << *pos << endl;
		*pos = 100;
		for (auto e : lt)
		{
			cout << e << " ";
		}
		cout << endl;
	}
	void test_list3()
	{
		list<int> lt;
		lt.push_back(1);
		lt.push_back(2);
		lt.push_back(3);
		lt.push_back(4);
		list<int>::iterator pos = find(lt.begin(), lt.end(), 2);
		if (pos != lt.end())
		{
			lt.erase(pos);
		}
		//这里的pos是否会失效？会失效，因为pos指向的节点已经被释放了，出现野指针
		cout << *pos << endl;
		*pos = 100;
	}
	//测试排序时间
	void TsetOP()
	{
		srand(time(0));
		const int N = 10000;
		list<int> lt;
		vector<int> v;
		v.resize(N);
		for (int i = 0; i < N; i++)
		{
			v[i] = rand();
			lt.push_back(v[i]);
		}
		int begin1 = clock();
		sort(v.begin(), v.end());
		int end1 = clock();

		int begin2 = clock();
		lt.sort();
		int end2 = clock();

		cout << "vector sort" << end1 - begin1 << endl;
		cout << "list sort" << end2 - begin2 << endl;
	}
}
int main()
{
	hzs::test_list1();
	//std::test_list2();
	//std::test_list3();
	std::TsetOP();
	return 0;
}