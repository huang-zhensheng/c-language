#define _CRT_SECURE_NO_WARNINGS 1
using namespace std;
#include<iostream>
//class Base1 
//{
//public: int _b1; 
//};
//class Base2
//{ 
//public: int _b2;
//};
//class Derive : public Base1, public Base2 
//{
//public: int _d;
//};
//int main()
//{
//	Derive d;
//	Base1* p1 = &d;
//	Base2* p2 = &d;
//	Derive* p3 = &d;
//	return 0;
//}
//class A{
//public:
//	A(char *s) { cout << s << endl; }
//	~A(){}
//};
//class B :virtual public A
//{
//public:
//	B(char *s1, char*s2) :A(s1) { cout << s2 << endl; }
//};
//class C :virtual public A
//{
//public:
//	C(char *s1, char*s2) :A(s1) { cout << s2 << endl; }
//};
//class D :public B, public C
//{
//public:
//	D(char *s1, char *s2, char *s3, char *s4) :B(s1, s2), C(s1, s3), A(s1)
//	{
//		cout << s4 << endl;
//	}
//};
//int main() {
//	D *p = new D("class A", "class B", "class C", "class D");
//	delete p;
//	return 0;
//}
//class Base
//{
//public:
//	virtual void Func1()
//	{
//		cout << "Base::Func1()" << endl;
//	}
//	virtual void Func2()
//	{
//		cout << "Base::Func2()" << endl;
//	}
//	void Func3()
//	{
//		cout << "Base::Func3()" << endl;
//	}
//	private:
//		int _b = 1;
//	};
//class Derive : public Base
//{
//public:
//	virtual void Func1()
//	{
//		cout << "Derive::Func1()" << endl;
//	}
//	virtual void Func4()
//	{
//		cout << "Derive::Func4()" << endl;
//	}
//private:
//	int _d = 2;
//};
//typedef void(*VFPTR)();
////打印虚表
//void PrintVFT(void* vft[])
//{
//	printf("%p\n",vft);
//	for (size_t i = 0; vft[i] != nullptr; i++)
//	{
//		printf("vft[%d]:%p->",i,vft[i]);
//		VFPTR f = (VFPTR)vft[i];
//		f();
//	}
//	printf("\n");
//}
//int main()
//{
//	Base bb;
//	int a = 0;
//	int* p1 = new int;
//	const char* p2 = "hello world";
//	auto pf = PrintVFT;
//	static int b = 1;
//	printf("栈祯变量:%p\n",&a);
//	printf("堆变量:%p\n", p1);
//	printf("常量区变量:%p\n", p2);
//	printf("函数地址变量:%p\n", pf);
//	printf("静态区变量:%p\n", &b);
//	printf("虚函数表地址:%p\n", *(int*)&bb);
//	return 0;
//}

//int main()
//{
//	Base b;
//	Derive d;
//	PrintVFT((void**)*(int*)&b);
//	PrintVFT((void**)*(int*)&d);
//
//	return 0;
//}



//class Person 
//{
//public:
//	virtual void BuyTicket() { cout << "买票-全价" << endl; }
//};
//class Student : public Person 
//{
//public:
//	virtual void BuyTicket() { cout << "买票-半价" << endl; }
//};
//void Func(Person& p)
//{
//	p.BuyTicket();
//}
////int main()
////{
////	Person Mike;
////	Func(Mike);
////	Student Johnson;
////	Func(Johnson);
////	return 0;
////}
//typedef void(*VFPTR)();
//void PrintVFT(VFPTR vft[])
//{
//	printf("%p\n", vft);
//	for (size_t i = 0; vft[i] != nullptr; ++i)
//	{
//		printf("vft[%d]:%p->", i, vft[i]);
//		vft[i]();
//	}
//	printf("\n");
//}
//
//int main()
//{
//	Base b;
//	Derive d;
//
//	PrintVFT((VFPTR*)(*(int*)&b));
//	PrintVFT((VFPTR*)(*(int*)&d));
//
//	return 0;
//}