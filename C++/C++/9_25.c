#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
void PrintSort(int*a, int n)
{
	for (int i = 0; i < n; i++)
	{
		printf("%d ", a[i]);
	}
	printf("\n");
}
_MergeSortNonR(int*a,int n)
{
	int* tmp = (int*)malloc(sizeof(int)*n);
	for (int groupNum = 1; groupNum < n; groupNum = groupNum * 2)
	{
		for (int i = 0; i < n; i = i + 2 * groupNum)
		{
			int begin1 = i; int end1 = i + groupNum - 1;
			int begin2 = i + groupNum; int end2 = i + groupNum * 2 - 1;
			int index = begin1;
			if (begin2 >= n)
			{
				begin2 = n + 1;
				end2 = n;
			}
			if (end1 >= n)
			{
				end1 = n - 1;

			}
			if (end2 >= n)
			{
				end2 = n - 1;
			}
			while (begin1 <= end1 && begin2 <= end2)
			{
				if (a[begin1] < a[begin2])
				{
					tmp[index] = a[begin1];
					index++;
					begin1++;
				}
				else
				{
					tmp[index] = a[begin2];
					index++;
					begin2++;
				}
			}
			while (begin1 <= end1)
			{
				tmp[index] = a[begin1];
				index++;
				begin1++;
			}
			while (begin2 <= end2)
			{
				tmp[index] = a[begin2];
				index++;
				begin2++;
			}
		}
		for (int i = 0; i < n; i++)
		{
			a[i] = tmp[i];
		}
		PrintSort(a, n);
	}
	free(tmp);
}
int main()
{
	int a[] = { 3, 4, 1, 6, 2 };
	int n = sizeof(a) / sizeof(int);
	_MergeSortNonR(a,n);
	PrintSort(a,n);
	return 0;
}