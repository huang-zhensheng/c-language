#define _CRT_SECURE_NO_WARNINGS 1
//#include<iostream>//头文件   
//using namespace std;//命名空间
//int main()
//{
//	cout << "hello world" << endl;//IO输出流
//	return 0;
//}


//using namespace std;这句话的作用是？
//#include<stdio.h>
//int a = 0;
////int a = 0;
////同一个作用域不能定义同名的
//int main()
//{
//	int a = 1;
//	return 0;
//}

//
//跟库的命名冲突了，C语言无法解决
//#include<stdio.h>
//#include<stdlib.h>//报错—>会发生冲突
//int a = 0;
//int rand = 10;
//int main()
//{
//	int a = 1;
//	printf("%d",rand);
//	return 0;
//}


//C++提出了命名空间来解决名字冲突的问题
//于是namespace诞生了
//#include<stdio.h>
//#include<stdlib.h>
//int a = 0;
//namespace hzs//可以在命名空间里面定义变量
//{
//	int rand = 10;
//}
//int main()
//{
//	int a = 1;
//	//printf("%d", rand);//访问全局的，打印出一个函数指针
//	//如何处理？
//	printf("%d",hzs::rand);//::域作用限定符，rand在左边bit的域里
//	return 0;
//}


////除了命名空间，还可以定义函数
//#include<stdio.h>
//#include<stdlib.h>
//namespace hzs//可以在命名空间里面定义变量
//{
//	//定义变量，定义函数，定义类型
//	int rand = 10;
//	int Add(int left, int right)
//	{
//		return left + right;
//	}
//	//还可以定义结构体
//	struct Node
//	{
//		struct Node*next;
//		int val;
//	};
//	//命名空间还可以嵌套定义
//	namespace  hzs2
//	{
//		int c;
//		int d;
//		int Sub(int left, int right)
//		{
//			return left - right;
//		}
//	}
//}
//////有了命名空间，他们互相之间就可以命名同名的东西
////namespace cpp
////{
////	int rand = 100;
////}
//int main()
//{
//	int a = 1;
//	printf("%d\n", rand);//访问全局的，打印出一个函数指针
//	//如何处理？
//	printf("%d\n",hzs::rand);//::域作用限定符，rand在左边bit的域里
//	//printf("%d\n", cpp::rand);
//	hzs::Add(1, 2);
//	struct hzs::Node node;
//	hzs::hzs2::Sub(1, 2);
//	return 0;
//}
//同一个工程中允许存在多个相同名称的命名空间，编译器最后会合成同一个命名空间中



////如何使用命名空间里面的东西？
////三种方式
////1，全部直接展开到全局
//using namespace std;//std是包含C++标准库的命名空间,优点：用起来方便，缺点：把自己的定义暴露出去，导致命名污染
//using namespace hzs;
//int main()
//{
//	int ret = Add(1, 2);
//	printf("%d",ret);
//	printf("%d", hzs::rand);
//	return 0;
//}

////2，访问每个命名空间中的东西时，制定命名空间
////优点：不存在命名污染，缺点，用起来麻烦，每个都得去指定命名空间
//std::rand

////3，把某个展开—>可以把常用的展开，不会造成大面积的污染
//using hzs::Node;
//using hzs::Add;
//int main()
//{
//	struct Node n1;
//	int ret = Add(1, 2);
//	printf("%d\n",ret);
//	printf("%d\n", hzs::rand);
//	//hzs::rand = 100;
//	return 0;
//}

//所以————>
//#include<iostream>
//using namespace std;
//int main()
//{
//	cout << "hello" << endl;
//	return 0;
//}
//#include<stdio.h>
//#include<stdlib.h>
//namespace N
//{
//	int a = 10;
//	int b = 20;
//	int Add(int left, int right)
//	{
//		return left + right;
//	}
//	int Sub(int left, int right)
//	{
//		return left - right;
//	}
//}
////int main()
////{
////	printf("%d\n", a); // 该语句编译出错，无法识别a
////	return 0;
////}
//int main()
//{
//	printf("%d\n", N::a);
//	printf("%d\n", N::b);
//	int ret = N::Add(10, 20);
//	printf("%d",ret);
//	return 0;
//}
//——————————————————————————————
//C++输入&输出
//#include<iostream>
//using namespace std;
//int main()
//{
//	int n;
//	cin >> n;//>>输入运算符/流提取运算符
//	int* a = (int*)malloc(sizeof(int)*n);
//	for (int i = 0; i < n; i++)
//	{
//		cin >> a[i];
//	}
//	for (int i = 0; i < n; i++)
//	{
//		cout << a[i]<<" ";//<<输出运算符/流插入运算符
//	}
//	return 0;
//}

//#include<iostream>
//using namespace std;
//int main()
//{
//	int n;
//	cin >> n;//>>输入运算符/流提取运算符
//	double* a = (double*)malloc(sizeof(double)*n);
//	for (int i = 0; i < n; i++)
//	{
//		cin >> a[i];
//	}
//	for (int i = 0; i < n; i++)
//	{
//		cout << a[i] << " ";//<<输出运算符/流插入运算符
//	}
//	return 0;
//}

//#include<iostream>
//using namespace std;
////自动识别类型
//
//struct Student
//{
//	char name[10];
//	int age;
//};
//int main()
//{
//	char ch = 'A';
//	int i = 10;
//	int* p = &i;
//	double d = 1.11111;//最多输出五位
//
//	//自动识别变量的类型
//	cout << ch << endl;
//	cout << i << endl;
//	cout << p << endl;
//	cout << d << endl;
//	printf("%.2f\n",d);
//
//	//类似下面的场景使用printf更好用
//	struct Student s = { "张三",18 };
//	cout << "名字:" << s.name << " " << "年龄:" << s.age << endl;
//	printf("名字:%s 年龄:%d\n",s.name,s.age);
//	return 0;
//}


//#include<iostream>
//using namespace std;
//void TestFunc(int a = 0)//缺省参数
//{
//	cout << a << endl;
//}
//int main()
//{
//	TestFunc();//不传a就用缺省的—等价于TestFunction(0)
//	TestFunc(10);//传了就没缺省参数什么用了
//	return 0;
//}
//
//#include<iostream>
//using namespace std;
////全缺省
////要传就按顺序传
//void TestFunc(int a = 10, int b = 20,int c = 30)
//{
//	cout << "a = " << a << endl;
//	cout << "b = " << b << endl;
//	cout << "c = " << c << endl;
//	cout << endl;
//}
//int main()
//{
//	TestFunc();
//	TestFunc(1);
//	TestFunc(1, 2);
//	TestFunc(1, 2, 3);
//}


//半缺省(部分缺省)
//半缺省 参数必须从右往左依次来给，不能间隔着给
//#include<iostream>
//using namespace std;
//void TestFunc(int a , int b = 20, int c = 30)
//{
//	cout << "a = " << a << endl;
//	cout << "b = " << b << endl;
//	cout << "c = " << c << endl;
//	cout << endl;
//}
//int main()
//{
//	//TestFunc();//不能不传
//	TestFunc(1);
//	TestFunc(1, 2);
//	TestFunc(1, 2, 3);
//}

//——————————————————————函数重载
//int Add(int left, int right)
//{
//	return left + right;
//}
//double Add(double left, double right)
//{
//	return left + right;
//}
//long Add(long left, long right)
//{
//	return left + right;
//}
//int main()
//{
//	Add(10, 20);
//	Add(10.0, 20.0);
//	Add(10L, 20L);
//	return 0;
//}

//为什么C++支持函数重载



