#include"string_2.h"
int main()
{
	try
	{
		bit::test_string1();
		bit::test_string2();
		bit::test_string3();
		bit::test_string4();
		bit::test_string5();
	}
	catch (exception& e)
	{
		cout << e.what() << endl;
	}
	return 0;
}

//class Solution
//{
//public:
//	string addStrings(string num1, string num2)
//	{
//		string s;
//		int carry = 0;
//		int end1 = num1.size() - 1;
//		int end2 = num2.size() - 1;
//		while (end1 >= 0 || end2 >= 0)
//		{
//			int val1 = 0;
//			int val2 = 0;
//			if (end1 >= 0)
//			{
//				val1 = num1[end1] - '0';
//				end1--;
//			}
//			if (end2 >= 0)
//			{
//				val2 = num2[end2] - '0';
//				end2--;
//			}
//			int ret = val1 + val2 + carry;
//			if (ret > 9)
//			{
//				ret -= 10;
//				carry = 1;
//			}
//			else
//			{
//				carry = 0;
//			}
//			s  += ('0' + ret);
//		}
//		if (carry == 1)
//		{
//			s += '1';
//		}
//		reverse(s.begin(), s.end());
//		return s;
//	}
//};


//class Solution
//{
//public:
//	string addStrings(string num1, string num2)
//	{
//		string s;
//		int end1 = num1.size() - 1;
//		int end2 = num2.size() - 1;
//		int carry = 0;//进位
//		while (end1 >= 0 || end2 >= 0)
//		{
//			int val1 = 0;
//			int val2 = 0;
//			if (end1 >= 0)
//			{
//				val1 = num1[end1] - '0';
//				end1--;
//			}
//			if (end2 >= 0)
//			{
//				val2 = num2[end2] - '0';
//				end2--;
//			}
//			int ret = val1 + val2 + carry;
//			if (ret > 9)
//			{
//				ret -= 10;
//				carry = 1;
//			}
//			else
//			{
//				carry = 0;
//			}
//			//retStr.insert(retStr.begin(), '0' + ret);
//			s += ('0' + ret);
//		}
//		if (carry == 1)
//		{
//			//retStr.insert(retStr.begin(), '1');
//			s += '1';
//		}
//		reverse(s.begin(), s.end());
//		return s;
//	}
//};


//class Solution
//{
//public:
//	bool IsletterOrNun(char ch)
//	{
//		if ((ch >= 'A' && ch <= 'Z')
//			|| (ch >= 'a'&& ch <= 'z')
//			|| (ch >= '0'&& ch <= '9'))
//		{
//				return true;
//		}
//		else
//		{
//			return false;
//		}
//	}
//	bool isPalindrome(string s)
//	{
//		int begin = 0;
//		int end = s.size() - 1;
//		while (begin < end)
//		{
//			while (begin < end && !IsletterOrNun(s[begin]))
//			{
//				begin++;
//			}
//			while (begin < end && !IsletterOrNun(s[end]))
//			{
//				end--;
//			}
//			if (s[begin] != s[end])
//			{
//				//有一个是数字，就不存在大小写比较问题
//				if (s[begin] < 'A' || s[end] < 'A')
//				{
//					return false;
//				}
//				//忽略字母的大小写
//				else if (s[begin] < s[end] && s[begin] + 32 == s[end])
//				{
//					begin++;
//					end--;
//				}
//				else if (s[end] < s[begin] && s[end] + 32 == s[begin])
//				{
//					begin++;
//					end--;
//				}
//				else
//				{
//					return false;
//				}
//			}
//			else
//			{
//				begin++;
//				end--;
//			}
//		}
//		return true;
//	}
//};