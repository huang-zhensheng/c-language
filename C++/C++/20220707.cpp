﻿#include <iostream>
#include <unordered_set>
#include <unordered_map>
#include <set>
#include <vector>
#include <time.h>
#include <string>
//#include <set>
using namespace std;

#include "HashTable.h"

void test_unordered_set()
{
	unordered_set<int> s;
	s.insert(3);
	s.insert(4);
	s.insert(5);
	s.insert(3);
	s.insert(1);
	s.insert(2);
	s.insert(6);

	unordered_set<int>::iterator it = s.begin();
	while (it != s.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;
}

void test_unordered_map()
{
	unordered_map<string, string> dict;
	dict.insert(make_pair("string", "ַ"));
	dict.insert(make_pair("sort", ""));

	auto it = dict.begin();
	while (it != dict.end())
	{
		cout << it->first << ":" << it->second << endl;
		++it;
	}
	cout << endl;
}

void test_op()
{
	set<int> s;
	unordered_set<int> us;
	const int n = 10000000;
	vector<int> v;
	srand(time(0));
	for (size_t i = 0; i < n; ++i)
	{
		v.push_back(rand());
	}

	size_t begin1 = clock();
	for (auto e : v)
	{
		s.insert(e);
	}
	size_t end1 = clock();

	size_t begin2 = clock();
	for (auto e : v)
	{
		us.insert(e);
	}
	size_t end2 = clock();

	cout << "set insert:" << end1 - begin1 << endl;
	cout << "unordered_set insert:" << end2 - begin2 << endl;
	cout << "=====================" << endl;
	size_t begin3 = clock();
	for (auto e : v)
	{
		s.find(e);
	}
	size_t end3 = clock();

	size_t begin4 = clock();
	for (auto e : v)
	{
		us.find(e);
	}
	size_t end4 = clock();

	cout << "set find:" << end3 - begin3 << endl;
	cout << "unordered_set find:" << end4 - begin4 << endl;

	cout << "=====================" << endl;
	size_t begin5 = clock();
	for (auto e : v)
	{
		s.erase(e);
	}
	size_t end5 = clock();

	size_t begin6 = clock();
	for (auto e : v)
	{
		us.erase(e);
	}
	size_t end6 = clock();

	cout << "set erase:" << end5 - begin5 << endl;
	cout << "unordered_set erase:" << end6 - begin6 << endl;
}

int main()
{
	//test_unordered_set();
	//test_unordered_map();
	//test_op();
	//close_hash::TestHashTable2();

	return 0;
}