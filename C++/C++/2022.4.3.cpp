#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;
int main()
{
	static int monthDays[13] = { 0 ,31,59,90,120,151,181,212,243,273,304,334,365};
	int year, month, day;
	cin >> year >> month >> day;
	int n = monthDays[month - 1] + day;
	if (month > 2 && (year % 4 == 0 && year % 100 != 0) || year % 400 == 0)
	{
		n++;
	}
	cout << n << endl;
	return 0;
}

//class A
//{
//public:
//	A(int a = 0)
//		:_a(a)
//	{}
//private:
//	int _a;
//};
//class B
//{
//private:
//	int _b = 0;
//	int* _p = (int*)malloc(sizeof(int)* 10);
//	A _aa = A(10);
//	//静态成员不能再这里给缺省值，因为静态成员不在构造函数初始化，要在类外面全局位置定义初始化
//	static int _n = 10;
//};
//int main()
//{
//	B bb;
//	return 0;
//}