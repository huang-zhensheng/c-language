#define _CRT_SECURE_NO_WARNINGS 1
using namespace std;
#include<iostream>
#include"Func.h"
int main()
{
	Stack<int> st1;
	return 0;
}
//template<class T1, class T2>
//class Data
//{
//public:
//	Data() 
//	{ 
//		cout << "Data<T1, T2>" << endl; 
//	}
//private:
//	T1 _d1;
//	T2 _d2;
//};
//template<class T1,class T2>
//class Data<T1*, T2*>
//{
//public:
//	Data() 
//	{ 
//		cout << "Data<T1*, T2*>" << endl; 
//	}
//private:
//};
//template<class T1, class T2>
//class Data<T1&, T2&>
//{
//public:
//	Data()
//	{
//		cout << "Data<T1&, T2&>" << endl;
//	}
//private:
//};
//void TestVector()
//{
//	Data<int*, int*> d1;
//	Data<double&, double&> d2;
//	Data<double, char> d3;
//	Data<int, char> d4;
//}
//int main()
//{
//	TestVector();
//	return 0;
//}
//template<class T>
//void Swap(T& a,T& b)
//{
//	T tmp = a;
//	a = b;
//	b = tmp;
//}
////模板匹配原则，进行特殊化处理
//void Swap(vector<int>& a, vector<int>& b)
//{
//	a.swap(b);
//}
////函数的模板特化
//template<>
//void Swap<vector<int>>(vector<int>& a, vector<int>& b)
//{
//	a.swap(b);
//}
//int main()
//{
//	int x = 10;
//	int y = 20;
//	Swap(x,y);
//
//	vector<int>v1 = { 1, 2, 3, 4 };
//	vector<int>v2 = { 10, 20, 30, 40 };
//	Swap(v1,v2);
//
//	return 0;
//}

//template<class T>
//bool IsEqual(const T& left, const T& right)
//{
//	return left == right;
//}
//// 函数模板的特化 （针对某些类型的特殊化处理）
//bool IsEqual(const char* const & left, const char* const & right)
//{
//	return strcmp(left, right) == 0;
//}
//int main()
//{
//	cout << IsEqual(1, 2) << endl;
//	char p1[] = "hello";
//	char p2[] = "hello";
//	cout << IsEqual(p1, p2) << endl;
//	return 0;
//}

//静态的栈
//#define N 10;

//T是类型模板参数
//N是非类型模板参数，N是一个常量
//template<class T,size_t N>
//class Stack
//{
//private:
//	T _a[N];
//	size_t _top;
//};
//int main()
//{
//	Stack<int,100>st1; //10
//	Stack<int, 1000>st1; //10
//	return 0;
//}
//类模板
//template<class T>
//class Stack
//{
//public:
//	Stack(int capacity = 4)
//		:_a(new T[capacity])
//		, _top(0)
//		, _capacity(capacity)
//	{}
//	~Stack()
//	{
//		delete[] _a;
//		_a = nullptr;
//		_top = _capacity = 0;
//	}
//	//类里面声明，类外面定义？
//	void Push(const T& x);
//private:
//	T* _a;
//	int _top;
//	int _capacity;
//};
//
////在类外面定义
//template<class T>
//void Stack<T>::Push(const T& x)
//{
//
//}
//int main()
//{
//	//类模板的使用都是显示实例化
//	Stack<TreeNode*>st1;
//	Stack<int>st2;
//}

