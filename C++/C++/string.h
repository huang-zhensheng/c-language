#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<string>
using namespace std;
#pragma once
namespace bit
{
	class string
	{
	public:
		string(char* str)
			:_str(new char[strlen(str)+1])
		{
			strcpy(_str, str);
		}
		char& operator[](size_t pos)
		{
		     return _str[pos];  
		} 
		//s2(s1)深拷贝
		string(const string& s)
			:_str(new char[strlen(s._str)+1])
		{
			strcpy(_str, s._str);
		}
		//深拷贝的现代写法
		/*string(const string& s)
			:_str(nullptr)
		{
			string tmp(s._str);
			swap(_str, tmp._str);
		}*/

		~string()
		{
			delete[] _str;
			_str = nullptr;
		}
		//s1 = s3
		/*string& operator=(const string& s)
		{
			if (this != &s)
			{
				delete[] _str;
				_str = new char[strlen(s._str) + 1];
				strcpy(_str, s._str);
			}
			return *this;
		}*/
		/*string& operator=(const string& s)
		{
			if (this != &s)
			{
				char* tmp = new char[strlen(s._str) + 1];
				delete[] _str;
				_str = tmp;
				strcpy(_str, s._str);
			}
			return *this;
		}*/
		//s1=s3
		/*string &operator = (const string &s)
		{
			if (this != &s)
			{
				string tmp(s._str);
				swap(_str, tmp._str);
			}
			return *this;
		}*/
		/*string& operator = (string s)
		{
			swap(_str, s._str);
			return *this;
		}*/
	private:
		char* _str;
	};
	void test_string1()
	{
		string s1("hello");
		s1[0] = 'x';
		string s2(s1);
		s2[0] = 'y';
		string s3("hello world");
		s1 = s3;
	}
}

