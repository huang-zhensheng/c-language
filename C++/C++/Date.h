using namespace std;
#include<iostream>
class Date
{
public:
	//获取合法的天数
	int GetMonthDay(int year,int month)
	{
		int monthDay[13] = { 0, 31,28, 31, 30, 31, 30, 31, 31, 30, 31, 30,31 };
		if (month == 2 && year % 4 == 0 && year % 100 != 0 || year % 400 == 0 )
		{
			return 29;
		}

		return monthDay[month];
	}

	Date(int year = 1, int month = 1, int day = 1)
	{
		_year = year;
		_month = month;
		_day = day;
	
		if (_year < 0 || _month <= 0 || _month >= 13 || _day <= 0 || _day > GetMonthDay(_year, _month))
		{
			cout << _year << "/" << _month << "/" << _day << "/" << "-->非法日期" << endl;
		}
	}	
	void Printf()
	{
		cout << _year << "/" << _month << "/" << _day << endl;
	}

	Date& operator+=(int day);
	Date operator+(int day);

	Date& operator-=(int day);
	Date operator-(int day);
private:
	int _year;
	int _month;
	int _day;
};
