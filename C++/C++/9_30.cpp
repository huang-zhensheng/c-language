#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<iostream>
using namespace std;

int Add(int left, int right)
{
	return left + right;
}
double Add(double left, double right)
{
	return left + right;
}
long Add(long left, long right)
{
	return left + right;
}
int main()
{
	Add(10, 20);
	Add(10.0, 20.0);
	Add(10L, 20L);
	return 0;
}








//#define Add(x,y)((x)+(y));
//int main()
//{
//	int ret  = Add(1, 2);
//	cout<< "ret = "<< ret << endl;
//	return 0; 
//}


//int main()
//{
//	int a = 1;
//	char b = 'a';
//	//通过右边赋值对象，自动推导变量类型
//	auto c = a;
//	auto d = b;
//	
//	//类型太复杂，太长，auto自动推导简化代码
//	//缺点:一定程度上牺牲了代码的可读性
//	return 0;
//}

//int main()
//{
//	int x = 10;
//	auto a = &x;
//	auto* b = &x;
//	auto& c = x;
//	cout << typeid(a).name() << endl;
//	cout << typeid(b).name() << endl;
//	cout << typeid(c).name() << endl;
//	*a = 20;
//	*b = 30;
//	c = 40;
//	return 0;
//}

//int main()
//{
//	int array[] = { 1, 2, 3, 4, 5 };
//	for (auto& e : array)
//	{
//		e *= 2;
//	}
//	for (auto e : array)
//	{
//		cout << e << " ";
//	}
//	cout << endl;
//	return 0;
//}