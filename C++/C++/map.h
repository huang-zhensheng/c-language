#define _CRT_SECURE_NO_WARNINGS 1
void test_map()
{
	string str[] = { "sort", "sort", "tree", "insert", "sort", "tree",  "test" };
	//统计次数方法1
	//map<string, int> countMap;
	//for (auto & e : str)
	//{
	//	auto ret = countMap.find(e);
	//	if (ret != countMap.end())
	//	{
	//		//(*ret).second++;
	//		ret->second++;
	//	}
	//	else
	//	{
	//		//countMap.insert(make_pair(e, 1));
	//		countMap.insert(pair<string, int>(e, 1));
	//	}
	//}
	//统计次数方法2
	//map<string, int>countMap;
	//for (auto& e : str)
	//{
	//	//pair<map<string,int>::iterator,bool> ret = countMap.insert(make_pair(e,1));
	//	auto ret = countMap.insert(make_pair(e, 1));
	//	if (ret.second == false)
	//	{
	//		ret.first->second++;
	//	}
	//}
	map<string, int> countMap;
	for (auto& e : str)
	{
		countMap[e]++;
	}
	map<string, int>::iterator it1 = countMap.begin();
	while (it1 != countMap.end())
	{
		cout << it1->first << ":"<<it1->second<<endl;
		it1++;
	}
	cout << endl;
}