#include"Date.h"
void TestDate1()
{
	Date d1(2021,10,13);
	Date d2 = d1 + 100;//��������
	//d1 += 100;//===>d1 = d1+100;
	d2.Print();
	d1.Print();
}
void TestDate2()
{
	Date d1(2021, 10, 13);
	Date d2 = d1 + 100;
	Date d3 = d1 + -100;
	d2.Print();
	d3.Print();
}
void TestDate3()
{
	Date d1(2021, 10, 13);
	Date d3 = --d1;
	d1.Print();
	d3.Print();
	Date d4 = d1--;
	d1.Print();
	d4.Print();
}
void TestDate4()
{
	Date d1(2021, 10, 13);
	Date d2(2021, 10, 31);
	Date d3(2023, 1, 1);
	cout << d2 - d1 << endl;
	cout << d3 - d1 << endl;
}
int main()
{
	//TestDate1();
	//TestDate2();
	//TestDate3();
	TestDate4();
	return 0;
}
