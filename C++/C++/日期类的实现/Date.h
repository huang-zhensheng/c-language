#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;
#include<assert.h>
class  Date
{
public:
	//获取某年某月的天数
	int GetMonthDay(int year,int month)
	{
		assert(month > 0 && month < 13);
		static int monthDays[13] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
		//闰年的2月是29天
		if (month == 2 && (year % 4 == 0 && year % 100 != 0) || year % 400 == 0)
		{
			return 29;
		}
		return monthDays[month];
	}
	//全缺省的构造函数
	Date(int year = 0, int month = 1, int day = 1)
	{
		_year = year;
		_month = month;
		_day = day;
		//判断日期是否合法
		if (_year < 0
			|| _month <=0 || _month >= 13
			|| _day <= 0 || _day > GetMonthDay(_year,_month))
		{
			cout << _year << "/" << _month << "/" << _day << "->"<<"非法日期"<<endl;
		}
	}
	//打印
	void Print()
	{
		cout << _year << "/" << _month << "/" << _day << endl;
	}
	Date operator+(int day);
	Date& operator+=(int day);


	Date operator-(int day);
	Date& operator-=(int day);

	int operator-(const Date& d);

	//++d1   d1.operator(&d1)
	Date& operator++();
	//d1++;
	//按正常的运算符重载规则，无法区分前置++和后置++重载
	//为了区分，这里做一个特殊处理，给后置++增加一个int参数，这样他们两个就构成函数重载
	Date operator++(int i);  //d1.operator(&d1,0)(这个参数仅仅是为了区分，这样他们两就构成函数重载)

	Date& operator--();
	Date operator--(int i);

	//>运算符重载
	bool operator > (const Date& d)
	{
		if (_year > d._year)
		{
			return true;
		}
		else if (_year == d._year && _month > d._month)
		{
			return true;
		}
		else if (_year == d._year && _month == d._month && _day > d._day)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	//==运算符重载
	bool operator==(const Date& d)
	{
		return _year == d._year
			&& _month == d._month
			&& _day == d._day;
	}
	//>=运算符重载
	bool operator >= (const Date& d)
	{
		return *this > d || *this == d;
	}
	//<运算符重载
	bool operator < (const Date& d)
	{
		return !(*this >= d);
	}
	//<=运算符重载
	bool operator <= (const Date& d)
	{
		return !(*this > d);
	}
	//!=运算符重载
	bool operator != (const Date& d)
	{
		return !(*this == d);
	}
private:
	int _year;
	int _month;
	int _day;
};