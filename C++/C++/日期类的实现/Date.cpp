#include"Date.h"
//日期+=天数
Date& Date::operator+=(int day)
{
	if (day < 0)
	{
		return *this -= -day;
	}
	_day += day;
	//日期不合法，进位
	while (_day > GetMonthDay(_year, _month))
	{
		_day -= GetMonthDay(_year, _month);
		_month++;
		if (_month == 13)
		{
			_year++;
			_month = 1;
		}
	}
	return *this;//出了作用域，d1还在

	//赋用+
	/**this = *this + day;
	return *this;*/
}
//日期+天数
Date Date::operator+(int day)
{
	/*Date tmp(*this);
	tmp._day += day;
	while (tmp._day > GetMonthDay(tmp._year, tmp._month))
	{
		tmp._day -= GetMonthDay(tmp._year, tmp._month);
		tmp._month++;
		if (tmp._month == 13)
		{
			tmp._year++;
			tmp._month = 1;
		}
	}
	return tmp;*/

	//赋用+=
	Date ret = *this;
	ret += day;
	return ret;
}
//日期-天数
Date Date::operator-(int day)
{
	Date ret = *this;
	ret -= day;
	return ret;
}
//日期-=天数
Date& Date::operator-=(int day)
{
	if (day < 0)
	{
		return *this += -day;
	}
	_day -= day;
	while (_day <= 0)
	{
		_month--;
		if (_month == 0)
		{
			_year--;
			_month = 12;
		}
		_day += GetMonthDay(_year, _month);
	}
	return *this;
}
//前置++
Date& Date::operator++()
{
	//_day += 1;
	////日期不合法，进位
	//if (_day > GetMonthDay(_year, _month))
	//{
	//	_day -= GetMonthDay(_year, _month);
	//	_month++;
	//	if (_month == 13)
	//	{
	//		_year++;
	//		_month = 1;
	//	}
	//}
	//赋用
	*this += 1;
	return *this;
}
//后置++
Date Date::operator++(int)
{
	Date tmp(*this);
	*this += 1;
	return tmp;
}
//前置--
Date& Date::operator--()
{
	//赋用
	*this -= 1;
	return *this;
}
//后置--
Date Date::operator--(int)
{
	Date tmp(*this);
	*this -= 1;
	return tmp;
}
//日期-日期   
int Date::operator-(const Date& d)
{
	Date max = *this, min = d;
	int flag = 1;
	if (*this < d)
	{
		max = d;
		min = *this;
		flag = -1;
	}
	int n = 0;
	while (min != max)
	{
		min++;
		n++;
	}
	return n*flag;
}