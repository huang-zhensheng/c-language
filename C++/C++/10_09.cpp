#define _CRT_SECURE_NO_WARNINGS 1
using namespace std;
#include<iostream>
class A
{
public:
	void PrintA()
	{
		cout << _a << endl;
	}
private:
	int _a;
};
int main()
{
	A* p = nullptr;
	p->PrintA();
}
//class Person
//{
//public:
//	void PrintPersonInfo();
//private:
//	char _name[20];
//	char _gender[3];
//	int _age;
//};
//// 这里需要指定PrintPersonInfo是属于Person这个类域
//void Person::PrintPersonInfo()
//{
//	cout << _name << " "<<_gender << " " << _age << endl;
//}
////人
//class Person
//{
//public:
//	//显示基本信息
//	void showInfo()
//	{
//		cout << _name << "-" << _sex << "-" << _age << endl;
//	}
//public:
//	char* _name;//姓名
//	char* _sex;//性别
//	int _age;//年龄
//};




//class Date
//{
//public:
//	//void Display(Data* this)
//	void Display()
//	{
//		cout << _year << "-" << _month << "-"<< _day << endl;
//	}
//	//void Init(Data*this,int year,int month,int day)
//	void Init(int year, int month, int day)
//	{
//		//成员函数里，我们不加的话，默认会在成员前面加this->
//		//我们也可以显示的在成员前面加this->
//		this->_year = year;
//		this->_month = month;
//		this->_day = day;
//
//		//对象可以调用成员函数，成员函数中还可以调用成员函数
//		//因为有this指针
//		this->Display();
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//int main()
//{
//	Date d1;
//	Date d2;
//	d1.Init(2021, 10, 8);//d1.Init(&d1,2021,10,8)
//	d2.Init(2022, 10, 8);//d1.Init(&d2,2022,10,8)
//	d1.Display();//d1.Display(&d1);
//	d2.Display();//d1.Display(&d2);
//	return 0;
//}

//class A
//{
//public:
//	void Print(){}
//private:
//	int i;
//	char ch;
//};
////没有成员变量的类
//class B
//{
//public:
//	void Print()
//	{}
//};
////空类
//class C
//{};
//int main()
//{
//	cout << sizeof(A) << endl;
//	cout << sizeof(B) << endl;
//	cout << sizeof(C) << endl;
//	return 0;
//}

//int main()
//{
//	cout << sizeof(A) << endl;
//	return 0;
//}

//C，关注的是过程
//namespace hzsc
//{
//	struct Stack
//	{
//		int*a;
//		int top;
//		int capacity;
//	};
//	void StackInit(struct Stack* ps){}
//	void StackPush(struct Stack* ps, int x){}
//}
////CPP
//namespace hzscpp
//{
//	class Stack
//	{
//		//访问限定符:
////访问权限作用域从该访问限定符出现的位置开始直到下一个访问限定符出现时为止
//		//想让你在类外面直接访问的定义成公有
//		//不想让你在类外面直接访问的定义成私有
//	public:
//		void Init(){}
//		void Push(int x){}
//		void Pop(){}
//		void Destory(){}
//		//...
//	private:
//		int*a;
//		int top;
//		int capacity;
//	};
//}
//int main()
//{
//	//C
//	struct hzsc::Stack stc;
//	hzsc::StackInit(&stc);
//	hzsc::StackPush(&stc,1);
//	hzsc::StackPush(&stc,2);
//	//CPP
//	hzscpp::Stack stcpp;
//    stcpp.Init();
//	stcpp.Push(1);
//	stcpp.Push(2);
//	return 0;
//}

//struct Student
//{
//	void SetStudentInfo(const char* name, const char* gender, int age)
//	{
//		strcpy(_name, name);
//		strcpy(_gender, gender);
//		_age = age;
//	}
//	void PrintStudentInfo()
//	{
//		cout << _name << " " << _gender << " " << _age << endl;
//	}
//	char _name[20];
//	char _gender[3];
//	int _age;
//};
//int main()
//{
//	Student s;
//	s.SetStudentInfo("Peter", "男", 18);
//	s.PrintStudentInfo();
//	return 0;
//}
//int main()
//{
//	return 0;
//}
//struct ListNodeC
//{
//	int val;
//	struct ListNodeC*next;
//};
//struct ListNodeCPP
//{
//	int val;
//	ListNodeCPP*next;
//};
//int main()
//{
//	struct ListNodeC n1;
//	ListNodeCPP n2;
//	return 0;
//}
