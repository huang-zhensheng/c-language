#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<string>
using namespace std;
//点类
class Point
{
public:
	//设置x
	void setX(int x)
	{
		m_X = x;
	}
	//获取x
	int getX()
	{
		return m_X;
	}
	//设置y
	void setY(int y)
	{
		m_Y = y;
	}
	//获取y
	int getY()
	{
		return m_Y;
	}
private:
	int m_X;
	int m_Y;
};
//圆类
class Circle
{
public:
	void setR(int r)
	{
		m_R = r;
	}
	int getR()
	{
		return m_R;
	}
	void setCenter(Point center)
	{
		m_Center = center;
	}
	Point getCenter()
	{
		return m_Center;
	}
private:
	int m_R;//半径
	Point m_Center;//圆心
};

void isInCircle(Circle& c,Point& p)
{
	//计算两点之间距离的平方
	int distance =
		(c.getCenter().getX() - p.getX())*(c.getCenter().getX() - p.getX())
		+ (c.getCenter().getY() - p.getY())*(c.getCenter().getY() - p.getY());
		//计算半径的平方
		int rDistance = c.getR()*c.getR();
	//判断
		if (distance == rDistance)
		{
			cout << "点在圆上" << endl;
		}
		else if (distance > rDistance)
		{
			cout << "点在圆外" << endl;
		}
		else
		{
			cout << "点在圆内" << endl;
		}
}
int main()
{
	//创建一个圆
	Circle c;
	c.setR(10);
	Point center;
	center.setX(0);
	center.setY(0);
	c.setCenter(center);
	//创建点
	Point p;
	p.setX(100);
	p.setY(0);
	//判断关系
	isInCircle(c, p);
	return 0;
}

//立方体类设计
//1，创建一个立方体类
//2,设计属性和行为
//3，设计行为 获取立方体的面积和体积
//4,分别利用全局函数和成员函数 判断两个立方体是否相等
//class Cube
//{
//public:
//	//设置长
//	void setL(int l)
//	{
//		m_L = l;
//	}
//	//获取长
//	int getL()
//	{
//		return m_L;
//	}
//	//设置宽
//	void setW(int w)
//	{
//		m_W = w;
//	}
//	//获取宽
//	int getW()
//	{
//		return m_W;
//	}
//	//设置高
//	void setH(int h)
//	{
//		m_H = h;
//	}
//	//获取高
//	int getH()
//	{
//		return m_H;
//	}
//	//获取立方体面积
//	int calculateS()
//	{
//		return 2 * m_L*m_W + 2 * m_L*m_H + 2 * m_H*m_W;
//	}
//	//获取立方体体积
//	int calculateV()
//	{
//		return m_L*m_H*m_W;
//	}
//	bool isSameByClass(Cube &c)
//	{
//		if (m_L == c.getL() && m_W == c.getW() && m_H == c.getH())
//		{
//			return true;
//		}
//		return false;
//	}
//private:
//	int m_L;
//	int m_W;
//	int m_H;
//};
//
//bool isSame(Cube& c1, Cube& c2)
//{
//	if (c1.getL() == c2.getL() && c1.getW() == c2.getW() && c1.getH() && c2.getH())
//	{
//		return true;
//	}
//	else
//	{
//		return false;
//	}
//}
//int main()
//{
//	Cube c1;
//	c1.setL(10);
//	c1.setW(10);
//	c1.setH(10);
//	Cube c2;
//	c2.setL(10);
//	c2.setW(100);
//	c2.setH(10);
//
//	bool ret = c1.isSameByClass(c2);
//	if (ret)
//	{
//		cout << "c1和c2是相等的" << endl;
//	}
//	else
//	{
//		cout << "c1和c2是不相等的" << endl;
//	}
//	cout << "c1的面积为：" << c1.calculateS() << endl;
//	cout << "c1的体积为：" << c1.calculateV() << endl;
//	return 0;
//}


//class Person
//{
//public:
//	//设置姓名
//	void setName(string name)
//	{
//		m_Name = name;
//	}
//	//获取姓名
//	string getName()
//	{
//		return m_Name;
//	}
//	//获取年龄
//	int getAge()
//	{
//		return m_Age;
//	}
//	//设置年龄
//	void setAge(int age)
//	{
//		if (age <0 || age>1000)
//		{
//			m_Age = 0;
//			cout << "输入有误" << endl;
//			return;
//		}
//		m_Age = age;
//	}
//	//设置情人  只写
//	void setLover(string lover)
//	{
//		m_Lover = lover;
//	}
//private:
//	//姓名  可读可写
//	string m_Name;
//	//年龄   只读
//	int m_Age;
//	//情人
//	string m_Lover;
//};
//int main()
//{
//	class Person p;
//	/*p.setName("张三");
//	cout << "姓名为：" << p.getName() << endl;
//	cout << "年龄为：" << p.getAge() << endl;
//	p.setLover("你好");*/
//	p.setAge(100);
//	cout << "年龄为：" << p.getAge() << endl;
//	return 0;
//}



//class和struct的区别
//struct 默认权限是公共
//class 默认权限是私有
//class C1
//{
//	int m_A;//默认权限是私有的
//};
//struct C2
//{
//	int m_A;//默认权限是公共权限
//};
//int main()
//{
//	return 0;
//}


//class person
//{
//public:
//	string m_Name;
//protected:
//	string m_Car;
//private:
//	int m_Password;
//public:
//	void func()
//	{
//		m_Name = "张三";
//		m_Car = "拖拉机";
//		m_Password = 123456;
//	}
//};
//int main()
//{
//	person p1;
//	p1.m_Name = "李四";
//	//p1.m_Car = "奔驰";//保护权限内容，在类外访问不到
//	//p1.m_Password = 123//私有权限内容，类外访问不到
//
//	return 0;
//}

//class Student
//{
//public:
//	//属性
//	string name;
//	int ID;
//	//行为
//	//显示姓名和学号
//	void showStudent()
//	{
//		cout << "姓名：" << name << endl << "学号：" << ID << endl;
//	}
//	//给姓名赋值
//	void setName(string _name)
//	{
//		name = _name;
//	}
//	void setID(int _ID)
//	{
//		ID = _ID;
//	}
//};
//int main()
//{
//	Student s1;
//	s1.setName("张三");
//	s1.setID(123456);
//	s1.showStudent();
//	s1.name = "李四";
//	s1.ID = 654321;
//	s1.showStudent();
//	return 0;
//}
//const double PI = 3.14;
//class Circle
//{
//	//访问权限public,公共权限
//public:
//	//属性
//	int m_r;
//	//行为
//	double calculate()
//	{
//		return 2 * PI*m_r;
//	}
//};
//int main()
//{
//	//通过圆类，创建具体的圆（对象）
//	Circle cl;
//	cl.m_r = 10;
//	cout << "圆的周长为：" << cl.calculate() << endl;
//	return 0;
//}
