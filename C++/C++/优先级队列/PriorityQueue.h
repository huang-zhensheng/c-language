#pragma once
#include<functional>
#include<vector>
#include<list>
namespace hzs
{
	//仿函数/函数对象——自定义类型
	//类型的对象可以像函数一样去使用
	template<class T>
	class Less
	{
	public:
		bool operator()(const T& x, const T& y)
		{
			return x < y;
		}
	};
	template<class T>
	class Greater
	{
	public:
		bool operator()(const T& x, const T& y)
		{
			return x > y;
		}
	};
	template<class T,class Container = vector<T>,class Compare = Less<typename Container value_type>>//容器适配器
	class priority_queue
	{
	public:
		priority_queue()
		{}
		template<class InputIterator>
		priority_queue(InputIterator first, InputIterator last)
		{
			//先把数据插入进去
			while (first != last)
			{
				_con.push_back(*first);
				first++;
			}
			for (int i = (_con.size() - 2) / 2; i >= 0; i--)
			{
				AdjustDown(i);
			}
		}
		//向上调整算法
		void AdjustUp(size_t child)
		{
			Compare com;
			size_t parent = (child - 1) / 2;
			while (child > 0)
			{
				//if (_con[parent] < _con[child])
				if (com(_con[parent] , _con[child]))
				{
					swap(_con[parent], _con[child]);
					child = parent;
					parent = (child - 1) / 2;
				}
				else
				{
					break;
				}
			}
		}
		void push(const T& x)
		{
			_con.push_back(x);
			AdjustUp(_con.size()-1);
		}
		//向下调整算法
		void AdjustDown(size_t parent)
		{
			Compare com;
			size_t child = parent * 2 + 1;//左孩子
			while (child < _con.size())
			{
				if (child + 1 < _con.size() && com(_con[child] ,_con[child + 1]))
				//if (child + 1 < _con.size() && _con[child] < _con[child + 1])
				{
					child++;
				}
				if (com(_con[parent] ,_con[child]))
				//if (_con[parent] < _con[child] )
				{
					swap(_con[child], _con[parent]);
					parent = child;
					child = parent * 2 + 1;
				}
				else
				{
					break;
				}
			}
		}
		void pop()
		{
			swap(_con[0], _con[_con.size() - 1]);
			_con.pop_back();
			AdjustDown(0);
		}
		const T& top()
		{
			return _con[0];
		}
		size_t size()
		{
			return _con.size();
		}
		bool empty()
		{
			return _con.empty();
		}
	private:
		Container _con;
	};
	void test_priority_queue2()
	{
		Less<int> less;
		cout << less(1, 2) << endl;
		//===>本质上是转换成调用 cout << less.operator()(1, 2) << endl;

		Greater<int> greater;
		cout << greater(1, 2) << endl;
	}
	void test_priority_queue()
	{
		//priority_queue<int> pq;//默认是大堆——大的优先级高
		priority_queue<int, vector<int>, Greater<int>> pq;//小堆
		pq.push(1);
		pq.push(6);
		pq.push(4);
		pq.push(3);
		pq.push(2);
		pq.push(5);
		cout << pq.top() << endl;
		while (!pq.empty())
		{
			cout << pq.top() << " ";
			pq.pop();
		}
		cout << endl;
	}
}