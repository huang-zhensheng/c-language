#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
//using namespace std;
////全局只要唯一的Singleton实例对象，那么他里面的成员也就是单例的
////饿汉模式:mian函数之前，一开始就创建对象
//class Singleton
//{
//public:
//	Singleton GetInstance()
//	{
//
//	}
//private:
//	Singleton()
//	{}
//private:
//	vector<int> _v;
//	static Singleton _sinst;
//};


//class NonInherit
//{
//public:
//	static NonInherit GetInstance()
//	{
//		return NonInherit();
//	}
//private:
//	NonInherit()
//	{}
//};
//C++98这种方式不够直接
//这里是可以继承的，但是Derive不能创建对象，因为Derive的构造函数
//必须调用父类NonInherit构造，但是NonInherit的构造函数私有了
//私有在子类中不可见，这里的继承不会报错，而继承的子类创建对象会报错
//class Derive :NonInherit
//{};
//int main()
//{
//	return 0;
//}

//请设计一个类，只能在堆上创建对象
//class OnlyHeap
//{
//public:
//	void DestoryObj()
//	{
//		delete this;
//	}
//private:
//	~OnlyHeap()
//	{
//		cout << "~OnlyHeap()" << endl;
//	}
//private:
//	int _a;
//};
//int main()
//{
//	OnlyHeap* ptr = new OnlyHeap;
//	ptr->DestoryObj();
//	return 0;
//}

//class OnlyHeap
//{
//public:
//	//2.提供一个静态的公有函数创建对象，对象创建的都在堆上
//	static OnlyHeap* CreateObj()
//	{
//		return new OnlyHeap;
//	}
//private:
//	//1.构造函数私有化
//	OnlyHeap()
//		:_a(0)
//	{}
//private:
//	int _a;
//	//C++98   防拷贝------只声明，不实现，声明为私有
//	OnlyHeap(const OnlyHeap& oh);
//};
//int main()
//{
//	//static突破类域
//	OnlyHeap* ptr = OnlyHeap::CreateObj();
//	OnlyHeap copy(*ptr);
//	delete ptr;
//	return 0;
//}

//设计一个类，只能在栈上创建对象
//请设计一个类，只能在栈上创建对象
//class StackOnly
//{
//public:
//	StackOnly()
//		:_a(0)
//	{}
//private:
//	//重载一个类专属operator new
//	//C++98 防调用---只声明，不实现，声明为私有
//	void* operator new(size_t size);
//	void operator delete(void* ptr);
//
//	//C++11
//	void* operator new(size_t size) = delete;
//	void operator delete(void* ptr) = delete;
//private:
//	int _a;
//};
//int main()
//{
//	StackOnly so;
//	StackOnly* ptr = new StackOnly;//报错
//	//这种方式存在一些漏洞，无法禁止在静态区创建对象
//	static StackOnly sso;
//	return 0;
//}