#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;
class Person 
{
public:
	virtual void BuyTicket()
	{
	cout << "买票-全价" << endl; 
	}
};
class Student : public Person
{
public:
	virtual void BuyTicket()
	{
	cout << "买票-半价" << endl; 
	}
};
void Func(Person& p)
{
	p.BuyTicket();
}
int main()
{
	Person Mike;
	Func(Mike);
	Student Johnson;
	Func(Johnson);
	return 0;
}
//class Base
//{
//public:
//	virtual void Func1()
//	{
//		cout << "Base::Func1()" << endl;
//	}
//	virtual void Func2()
//	{
//		cout << "Base::Func2()" << endl;
//	}
//	void Func3()
//	{
//		cout << "Base::Func3()" << endl;
//	}
//private:
//	int _b = 1;
//};
//class Derive : public Base
//{
//public:
//	virtual void Func1()
//	{
//		cout << "Derive::Func1()" << endl;
//	}
//private:
//	int _d = 2;
//};
//int main()
//{
//	Base b;
//	Derive d;
//	return 0;
//}
//class Base
//{
//public:
//	virtual void Func1()
//	{
//		cout << "Func1()" << endl;
//	}
//private:
//	int _b = 1;
//};
//int main()
//{
//	Base b;
//	cout << sizeof(Base) << endl;
//	return 0;
//}

//class Car
//{
//public:
//	virtual void Drive() = 0;
//};
//class Benz :public Car
//{
//public:
//	virtual void Drive()
//	{
//		cout << "Benz-舒适" << endl;
//	}
//};
//class BMW :public Car
//{
//public:
//	virtual void Drive()
//	{
//		cout << "BMW-操控" << endl;
//	}
//};
//void Test()
//{
//	Car* pBenz = new Benz;
//	pBenz->Drive();
//	Car* pBMW = new BMW;
//	pBMW->Drive();
//}
//int main()
//{
//	Test();
//}
//class Car{
//public:
//	virtual void Drive(){}
//};
//class Benz :public Car {
//public:
//	virtual void Drive() override { cout << "Benz-舒适" << endl; }
//};
//class Car
//{
//public:
//	virtual void Drive() final {}
//};
//class Benz :public Car
//{
//public:
//	virtual void Drive() 
//	{ cout << "Benz-舒适" << endl; }
//};
//int main()
//{
//	return 0;
//}

//class Person
//{
//public:
//	virtual ~Person()
//	{
//		cout << "~Person()" << endl;
//	}
//};
//class Student :public Person
//{
//public:
//	virtual ~Student()
//	{
//		cout << "~Student()" << endl;
//	}
//};
////如果不是虚函数，他们之间是隐藏关系
////是虚函数，他们是重写关系
//int main()
//{
//	////普通场景下面，虚函数是否重写都是OK的
//	//Person p;
//	//Student s;
//	//new对象特殊场景
//	Person* p1 = new Person;
//	Person* p2 = new Student;
//
//	delete p1;//p1->destructor()+operator(p1)
//	delete p2;//p2->destructor()+operator(p2)
//	return 0;
//}

//class A{};
//class B : public A {};
//class Person {
//public:
//	virtual A* f() { return new A; }
//};
//class Student : public Person {
//public:
//	virtual B* f() { return new B; }
//};
//int main()
//{
//	return 0;
//}
//class Person
//{
//public:
//	virtual void BuyTicket()
//	{
//		cout << "正常排队—全价买票" << endl;
//	}
//protected:
//	int _age;
//	string _name;
//};
//class Student:public Person
//{
//public:
//	virtual void BuyTicket()
//	{
//		cout << "正常排队—半价买票" << endl;
//	}
//protected:
//	int _age;
//	string _name;
//};
//class Solider:public Person
//{
//public:
//	virtual void BuyTicket()
//	{
//		cout << "优先排队—全价买票" << endl;
//	}
//protected:
//	int _age;
//	string _name;
//};
////多态两个条件
////1,子类重写父类的虚函数
////2,必须是父类的指针或者引用去调用虚函数
//void Func(Person& ptr)
//{
//	//多态—ptr指向父类对象调用父类的虚函数，指向子类对象调子类虚函数
//	ptr.BuyTicket();
//}
//int main()
//{
//	Person ps;
//	Student st;
//	Solider sd;
//
//	Func(ps);
//	Func(st);
//	Func(sd);
//}



//class A
//{
//public:
//	int _a;
//};
//// class B : public A
//class B : virtual public A
//{
//public:
//	int _b;
//};
//// class C : public A
//class C : virtual public A
//{
//public:
//	int _c;
//};
//class D : public B, public C
//{
//public:
//	int _d;
//};
//int main()
//{
//	D d;
//	d.B::_a = 1;
//	d.C::_a = 2;
//	d._b = 3;
//	d._c = 4;
//	d._d = 5;
//	d._a = 0;
//	return 0;
//}