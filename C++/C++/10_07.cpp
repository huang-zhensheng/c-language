#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<math.h>
#include<stdlib.h>
//struct BinaryTreeNode
//{
//	int val;
//	BinaryTreeNode* left;
//	BinaryTreeNode* right;
//};

//Definition for singly-linked list.
//struct ListNode
//{
//	int val;
//	struct ListNode *next;
//};
//struct ListNode* removeElements(struct ListNode* head, int val)
//{
//	if (head == NULL)
//		return NULL;
//	struct ListNode* newnode = NULL;
//	struct ListNode* tail = NULL;
//	//取不是val的节点尾插
//	struct ListNode* cur = head;
//
//	while (cur)
//	{
//		struct ListNode* next = cur->next;
//		if (cur->val == val)
//		{
//			free(cur);
//		}
//		else
//		{
//			//尾插
//			if (tail == NULL)
//			{
//				newnode = tail = cur;
//			}
//			else
//			{
//				//这里顺序注意一下
//				tail->next = cur;
//				tail = cur;
//			}
//		}
//		cur = next;
//	}
//	if (tail)
//		tail->next = NULL;
//	return newnode;
//}
//void reverse(struct ListNode*n1)
//{
//	if (n1 != NULL)
//	{
//		if (n1->next != NULL)
//		{
//			reverse(n1->next);
//		}
//		printf("%d->",n1->val);
//	}
//}
//int main()
//{
//	struct ListNode*n1 = (struct ListNode*)malloc(sizeof(struct ListNode));
//	struct ListNode*n2 = (struct ListNode*)malloc(sizeof(struct ListNode));
//	struct ListNode*n3 = (struct ListNode*)malloc(sizeof(struct ListNode));
//	struct ListNode*n4 = (struct ListNode*)malloc(sizeof(struct ListNode));
//	n1->val = 1;
//	n2->val = 2;
//	n3->val = 3;
//	n4->val = 7;
//
//	n1->next = n2;
//	n2->next = n3;
//	n3->next = n4;
//	n4->next = NULL;
//	/*struct ListNode* newnode = removeElements(n1, 7);
//	while (newnode != NULL)
//	{
//		printf("%d->", newnode->val);
//		newnode = newnode->next;
//	}*/
//	//reverse(n1);
//	struct ListNode*newnode = n1;
//	while (newnode)
//	{
//		printf("%d->",newnode->val);
//		newnode = newnode->next;
//	}
//	return 0;
//}
//void merge(int*num1,int*num2,int n1,int n2)
//{
//	int i1 = n1 - 1;
//	int i2 = n2 - 1;
//	int dest = 2;
//	while (dest>=0 && i2>=0)
//	{
//		if (num1[dest] > num2[i2])
//		{
//			num1[i1] = num1[dest];
//			dest--;
//			i1--;
//		}
//		else
//		{
//			 num1[i1] = num2[i2];
//			 i2--;
//			 i1--;
//		}
//	}
//	//num1还有剩余，不用处理，num2还有剩余，拷贝上去
//	while (i2>=0)
//	{
//		 num1[i1] = num2[i2];
//		 i2--;
//		 i1--;
//	}
//}
//void print(int*arr,int m)
//{
//	for (int i = 0; i < m; i++)
//	{
//		printf("%d  ",arr[i]);
//	}
//}
//int main()
//{
//	int num1[] = { 3, 5, 7, 0, 0, 0 };
//	int num2[] = { 2, 6, 11 };
//	int n1 = sizeof(num1) / sizeof(int);
//	int n2 = sizeof(num2) / sizeof(int);
//	merge(num1,num2,n1,n2);
//	print(num1,n1);
//	return 0;
//}
//bool Find(int arr[4][4],int row,int col,int number)
//{
//	int rows = 0;
//	int cols = col - 1;
//	while (rows < row && cols >= 0)
//	{
//		if (arr[rows][cols] == number)
//		{
//			return true;
//		}
//		else if (arr[rows][cols] > number)
//		{
//			cols--;
//		}
//		else
//		{
//			rows++;
//		}
//	}
//	return false;
//}
//int main()
//{
//	int arr[4][4] = { 1, 2, 8, 9, 2, 4, 9, 12, 4, 7, 10, 7, 6, 8, 11, 15 };
//	bool ret = Find(arr, 4, 4, 7);
//	if (ret == 1)
//		printf("ture");
//	else
//		printf("false");
//	return 0;
//}
//左右指针法
//void swap(int*x, int*y)
//{
//	int tmp = *x;
//	*x = *y;
//	*y = tmp;
//}
//int GetMidIndex(int*a,int left,int right)
//{
//	int mid = left + (right - left) / 2;
//	if (a[left] < a[right])
//	{
//		if (a[mid] < a[left])
//		{
//			return left;
//		}
//		else if (a[mid] > a[right])
//		{
//			return right;
//		}
//		else
//		{
//			return mid;
//		}
//	}
//	else//a[left] > a[right]
//	{
//		if (a[mid] > a[left])
//		{
//			return left;
//		}
//		else if (a[mid] < a[right])
//		{
//			return right;
//		}
//		else
//		{
//			return mid;
//		}
//	}
//}
//int PartSort(int*a, int left,int right)
//{
//	int keyi = left;
//	int prev = left;
//	int cur = prev + 1;
//	while (cur <= right)
//	{
//		if (a[cur] < a[keyi])
//		{
//			prev++;
//			swap(&a[cur],&a[prev]);
//			cur++;
//		}
//		else
//		{
//			cur++;
//		}
//	}
//	swap(&a[keyi],&a[prev]);
//	return prev;
//}
//void QuickSort(int* a,int left,int right)
//{
//	if (left < right)
//	{
//		int keyi = PartSort(a, left, right);
//		QuickSort(a, left, keyi - 1);
//		QuickSort(a, keyi+1, right);
//	}
//}
//int main()
//{
//	int arr[] = { 8, 6, 5, 3, 4, 9, 7, 1, 2, 10 };
//	int n = sizeof(arr) / sizeof(int);
//	int left = 0;
//	int right = n - 1;
//	int ret = PartSort(arr,left,right);
//	QuickSort(arr, left, right);
//	for (int i = 0; i < n; i++)
//	{
//		printf("%d  ",arr[i]);
//	}
//	printf("\n");
//	printf("%d",ret);
//	return 0;
//}


//int Fib(int a,int b,int c,int N)
//{
//	while (1)
//	{
//		if (N == b)
//		{
//			return 0;
//		}
//		else if (N < b)
//		{
//			if (abs(N - b) > abs(N - a))
//			{
//				return abs(N - a);
//			}
//			else
//			{
//				return abs(N - b);
//			}
//		}
//		else
//		{
//			c = a + b;
//			a = b;
//			b = c;
//		}
//	}
//}
//int main()
//{
//	int a = 0;
//	int b = 1;
//	int c = 0;
//	int N = 0;
//	scanf("%d",&N);
//	int ret = Fib(a,b,c,N);
//	printf("%d",ret);
//	return 0;
//}