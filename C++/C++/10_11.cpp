#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;
class Date
{
public:
	Date(int year, int month, int day)
	{
		_year = year;
		_month = month;
		_day = day;
	}
	Date(const Date& d)
	{
		_year = d._year;
		_month = d._month;
		_day = d._day;
	}
	void Print()
	{
		cout << _year << "/" << _month << "/" << _day << endl;
	}
private:
	int _year;
	int _month;
	int _day;
};
int main()
{
	Date d1(2021,10,9);
	Date d2(d1);
	d1.Print();
	return 0;
}

//class Date
//{
//public:
//	Date(int year = 1,int month = 1, int day = 1)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//	void Print()
//	{
//		cout << _year << "/" << _month << "/" << _day << endl;
//	}
//	//像Date这样的类是不需要析构函数的，因为他内部没有什么资源需要清理
//	~Date()
//	{
//		cout << "~Date()" << endl;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//class Stack
//{
//public:
//	Stack(int capacity = 4)
//	{
//		_a = (int*)malloc(sizeof(int)*capacity);
//		if (_a == nullptr)
//		{
//			cout << "malloc fail" << endl;
//		}
//		_top = 0;
//		_capacity = capacity;
//	}
//	~Stack()
//	{
//		//像Stack这样的类，对象中的资源需要清理，就用析构函数
//		free(_a);
//		_a = nullptr;
//		_top = _capacity = 0;
//	}
//private:
//	int* _a;
//	int _top;
//	int _capacity;
//};
//int main()
//{
//	Date d1;
//	Stack st;
//	return 0;
//}


//class Date
//{
//public:
//	void Print()
//	{
//		cout << _year << "/" << _month << "/" << _day << endl;
//	}
//	~Date()
//	{
//		cout << "~Date()" << endl;
//	}
//	//像Date这样的类是不需要析构函数的，因为他内部没有什么资源需要清理
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//int main()
//{
//	Date d1;
//	return 0;
//}

//class Date
//{
//public:
//	Date(int year)
//	{
//		this->year = year;
//	}
//private:
//	int year;
//};
//int main()
//{
//	Date d(1);
//}

//class A
//{
//public:
//	A()
//	{
//		_a1 = 0;
//		_a2 = 1;
//	}
//private:
//	int _a1;
//	int _a2;
//};
//class Date
//{
//public:
//	void Print()
//	{
//		cout << _year << "/" << _month << "/" << _day << endl;
//	}
//private:
//	//内置类型/基本类型    int,char,double，指针......
//	int _year;
//	int _month;
//	int _day;
//	//自定义类型
//	A _aa;
//};
//int main()
//{
//	Date d1;
//	d1.Print();
//	return 0;
//}


//class A
//{
//public:
//	A()
//	{
//		_a1 = 0;
//		_a2 = 1;
//		cout << "A()" << endl;
//	}
//private:
//	int _a1;
//	int _a2;
//};
//class Date
//{
//public:
//	//本来是构成函数重载的
//	//但是他们两不能同时存在！！！
//	/*Date()
//	{
//		_year = 0;
//		_month = 1;
//		_day = 1;
//	}*/
//	Date(int year = 0,int month = 1,int day = 1)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//	void Print()
//	{
//		cout << _year << "/" << _month << "/" << _day << endl;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//int main()
//{
//	Date d1;
//	Date(1, 1, 1);
//	return 0;
//}