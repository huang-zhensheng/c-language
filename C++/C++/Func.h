#pragma once
#include<iostream>
using namespace std;
template<class T>
class Stack
{
public:
	Stack();
	~Stack();
	void Push(const T& x);
private:
	T* _a;
	int _top;
	int _capacity;
};

template<class T>
Stack<T>::Stack()
{
	_a = new T[10];
	_top = 0;
	_capacity = 10;
}
template<class T>
Stack<T>::~Stack()
{
	delete[] _a;
	_a = nullptr;
}

template<class T>
void Stack<T>::Push(const T & x)
{
	_a[_top] = x
	++_top;
}

//template<class T>
//void F(const T& x);
//
//template<class T>
//void F(const T& x)
//{
//	cout << "void F(const T& x)" << endl;
//}
//void Print();