#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;
class A
{
private:
	static int k;
	int h;
public:
	//B天生就是A的友元
	class B
	{
	public:
		void foo(const A& a)
		{
			cout << k << endl;//OK
			cout << a.h << endl;//OK
		}
	};
};
int A::k = 1;
int main()
{
	A::B b;
	b.foo(A());
	return 0;
}
//class A
//{
//public:
//	A(int a = 0)
//	:_a(a)
//	{}
//private:
//	int _a;
//};
//class B
//{
//public:
//	B()
//		: _p(nullptr)
//		, _aa(100)
//	{}
//private:
//	//给的是缺省值，这里不是定义
//	//这里只是声明，所以这里不是初始化
//	int _b = 0;
//	int* _p = (int*)malloc(sizeof(int)* 10);
//	A _aa = 10;
//	//静态变量不能在这里给缺省值，因为静态成员不在构造函数初始化
//	//要在类外面全局位置定义初始化
//	//static int _n = 10;
//};
//int main()
//{
//	B bb;
//	return 0;
//}

//class Date
//{
//	//友元函数
//	friend ostream& operator<<(ostream& out, const Date& d);
//	friend istream& operator>>(istream& in, Date& d);//这里不能加const
//public:
//	Date(int year = 0, int month = 0, int day = 1)
//		:_year(year)
//		, _month(month)
//		, _day(day)
//	{}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
////为了让cout在第一个参数，左左操作数，我们就只能写成全局的
////其次就是operator<<搞成友元，可以在类中访问私有，但是operator<<不是必须友元，还有其他方式
//ostream& operator<<(ostream& out, const Date& d)
//{
//	out << d._year << "/" << d._month << "/" << d._day << endl;
//	return out;
//}
//istream& operator>>(istream& in, Date& d)//这里不能加const
//{
//	in >> d._year >> d._month >> d._day;
//	return in;
//}
//int main()
//{
//	Date d1,d2;
//	//cin >> d1;//流提取
//	//cout << d1;//流插入
//	//运算符重载，运算符有几个操作数,重载函数就有几个参数
//	//如果是两个操作数，左操作数是第一个参数，右操作数是第二个参数
//	//operator<<写成成员函数，this指针默认占据了第一个位置，对象就要做做操作数
//	//那么用起来就不符合流特性，虽然可以用，但是不符合运算符原来的用法和特性
//	/*d1.operator<<(cout);
//	d1 << cout;*/
//	cin >> d1 >> d2;
//	cout << d1 << d2;
//	return 0;
//}


//int countC = 0;
//int countCC = 0;
//class A
//{
//public:
//	A()
//	{
//		_count++;
//	}
//	A(const A& a)
//	{
//		_count++;
//	}
//	//静态成员函数
//	//静态成员函数没有this指针，无法访问_a
//	static int GetCount()
//	{
//		return _count;
//	}
//private:
//	//声明
//	int _a;            //存在定义出的对象中，属于某个对象
//	static int _count;//存在静态区，属于整个类，也属于每个定义出来的对象共享
//	                  //跟全局变量比较，他受类域和访问限定符限制，更好的体现封装，别人不能轻易的修改
//};
////静态成员变量不能在构造函数初始化，在全局位置定义初始化
//int A::_count = 0;
//
//A f(A a)
//{
//	A ret(a);
//	return ret;
//}
//int main()
//{
//	A a1 = f(A());
//	A a2;
//	A a3;
//	a3 = f(a2);
//	cout << sizeof(A) << endl;
//	//单纯的只想用一个对象去调用
//	A ret;
//	cout << ret.GetCount()-1 << endl;
//	//下面就是匿名对象的一种使用场景
//	cout << A().GetCount() - 2 << endl;//A()匿名对象
//	cout << A::GetCount()-2 << endl;//使用静态的成员函数突破类域进行调用
//	return 0;
//}

//class A
//{
//public:
//	A(int a)
//		:_a(a)
//	{
//		cout << "A(int a)" << endl;
//	}
//	A(const A& aa)
//	{
//		cout << "A(const A& aa)" << endl;
//	}
//private:
//	int _a;
//};
//int main()
//{
//	A aa1(1);
//	A aa2 = 2;
//	return 0;
//}
//class A
//{
//public:
//	A(int a)
//		:_a1(a)
//		, _a2(_a1)
//	{}
//	void Print() {
//		cout << _a1 << " " << _a2 << endl;
//	}
//private:
//	int _a2;
//	int _a1;
//};
//int main() {
//	A aa(1);
//	aa.Print();
//}

//class B
//{
//public:
//	B(int c,int ref)
//		:_c(c)
//		, _ref(ref)
//	{
//		_ref = ref;
//	}
//private:
//	const int _c;
//	int& _ref;
//};
//int main()
//{
//	int ref = 10;
//	B b(1,ref);
//	cout << ref << endl;
//	return 0;
//}


//class A
//{
//public:
//	A(int a =0)
//	{
//		_a = a;
//		cout << "A(int a = 0)" << endl;
//	}
//	A& operator = (const A& aa)
//	{
//		cout << "A& operator = (const A& aa)" << endl;
//		if (this != &aa)
//		{
//			_a = aa._a;
//		}
//		return *this;
//	}
//private:
//	int _a;
//};
//class B
//{
//public:
//	B(int a, int b)
//		:_aa(a)
//	{
//		_b = b;
//	}
//private:
//	int _b = 1;//内置类型
//	A _aa;//自定义类型
//};
//int main()
//{
//	B b(10,20);
//	return 0;
//}
//class A
//{
//public:
//	A()
//	{
//		cout << "A()" << endl;
//	}
//	~A()
//	{
//		cout << "~A()" << endl;
//	}
//};
//class B
//{
//public:
//	B()
//	{
//		cout << "B()" << endl;
//	}
//	~B()
//	{
//		cout << "~B()" << endl;
//	}
//};
//class C
//{
//public:
//	C()
//	{
//		cout << "C()" << endl;
//	}
//	~C()
//	{
//		cout << "~C()" << endl;
//	}
//};
//class D
//{
//public:
//	D()
//	{
//		cout << "C()" << endl;
//	}
//	~D()
//	{
//		cout << "~D()" << endl;
//	}
//};
//C c;
//int main()
//{
//	A a;
//	B b;
//	static D d;
//	return 0;
//}

//class A
//{
//public:
//    A(int a)
//		:_a(a)
//	{
//		cout << "A(int a)" << endl;
//	}
//	A(const A& aa)
//	{
//		cout << "A(const A& aa)" << endl;
//	}
//private:
//	int _a;
//};
//int main()
//{
//	A aa(1);
//	A aa2 = 2;//隐式类型转换
//	return 0;
//}
//class A
//{
//public:
//	A(int a = 0)
//	{
//		_a = a;
//		cout << "A(int a = 0)" << endl;
//	}
//	A& operator = (const A& aa)
//	{
//		cout << "A& operator = (const A& aa)" << endl;
//		if (this != &aa)
//		{
//			_a = aa._a;
//		}
//		return *this;
//	}
//private:
//	int _a;
//};
//class B
//{
//public:
//	B(int a, int b)
//		:_aa(a)
//		, _b(b)
//	{
//
//	}
//private:
//	int _b = 1;
//	A _aa;
//};
//int main()
//{
//	B b(10, 20);
//	return 0;
//}

//class B
//{
//	B(int c, int ref)
//	:_c(c)
//	, _ref(ref)
//};
//private:
//	const int _c;
//	int& _ref;
//
//int main()
//{
//	int ref = 10;
//	B b(1, ref);
//	cout << ref << endl;
//	return 0;
//}
//class A
//{
//public:
//	A(int a = 0)
//	{
//		_a = a;
//	}
//private:
//	int _a;
//};
//class B
//{
//public:
//	B(int a, int b)
//	{
//		/*A aa(a);
//		_aa = aa;*/
//		_aa = A(a);
//		_b = b;
//	}
//private:
//	int _b = 1;//内置类型
//	A _aa;//自定义类型
//};
//int main()
//{
//	B b(20,10);
//	return 0;
//}