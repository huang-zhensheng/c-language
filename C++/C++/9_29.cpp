#define _CRT_SECURE_NO_WARNINGS 1
//#include<iostream>
//using namespace std;
//int main()
//{
//	//const引用
//	const int a = 10;
//	//int& b = a;//变成你的别名，还能修改你（不行）
//	int c = 20;
//	const int& d = c;//变成你的别名，不能修改你（行）
//	return 0;
//}
#include<iostream>
using namespace std;
int& Add(int a, int b)
{
	int c = a + b;
	return c;
}
int main()
{
	int& ret = Add(1, 2);
	//Add(3, 4);
	printf("hello\n");
	cout << "Add(1,2) is:" << ret << endl;
	return 0;
}


//int main()
//{
//	int i = 10;
//	double d = i;
//	const double& r = i;
//	return 0;
//}
//int Swap(int& rx,int& ry)
//{
//	int tmp = rx;
//	rx = ry;
//	ry = tmp;
//	return ry;
//}
//int main()
//{
//	int x = 0;
//	int y = 1;
//	int*p1 = &x;
//	int*p2 = &y;
//	int*&p3 = p1;
//	*p3 = 10;
//	p3 = p2;
//	return 0;
//}
//int main()
//{
//	//注意：这里跟C取地址用了一个符号&
//	//之间无关联，各作各的用处
//	/*int a = 10;
//	int& b = a;
//	int& c = a;
//	int& d = b;*/
//
//	int a = 10;
//	int& b = a;
//	int c = 20;
//	b = c;
//	return 0;
//}