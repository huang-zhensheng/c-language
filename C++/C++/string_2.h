#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<string>
#include<assert.h>
#pragma once
using namespace std;
namespace bit
{
	class string
	{
	public:
		//迭代器
		typedef char*iterator;
		typedef const char* const_iterator;

		iterator begin()
		{
			return _str;
		}
		iterator end()
		{
			return _str + _size;
		}
		const_iterator begin() const
		{
			return _str;
		}
		const_iterator end() const
		{
			return _str + _size;
		}

		//默认构造函数
		/*string()
			:_str(new char[1])
			, _size(0)
			, _capacity(0)
		{
			*_str = '\0';
		}*/
		string(char* str = "\0")
			:_size(strlen(str))
			, _capacity(_size)
		{
			_str = new char[_capacity + 1];
			strcpy(_str, str);
		}
		//读写
		char& operator[](size_t pos)
		{
			assert(pos < _size);
			return _str[pos];//*(_str + pos)
		}
		//只读  const对象
		const char& operator[](size_t pos) const
		{
			assert(pos < _size);
			return _str[pos];//*(_str + pos)
		}
		//s.swap(s2)
		void swap(string& s)
		{
			//全局域的swap
			::swap(_str, s._str);
			::swap(_size, s._size);
			::swap(_capacity, s._capacity);
		}
		//深拷贝的现代写法 s2(s1)
		string(const string& s)
			:_str(nullptr)
		{
			string tmp(s._str);
			this->swap(tmp);
			//swap(_str, tmp._str);
		}
		//赋值的现代写法 s1=s3
		string& operator = (string s)
		{
			this->swap(s);
			//swap(_str, s._str);
			return *this;
		}
		~string()
		{
			delete[] _str;
			_str = nullptr;
		}
		//开空间
		void reserve(size_t n)
		{
			if (n > _capacity)
			{
				char* tmp = new char[n + 1];
				strcpy(tmp, _str);
				delete[] _str;
				_str = tmp;

				_capacity = n;
			}
		}
		//给n个字符初始化
		void resize(size_t n, char ch = '\0')
		{
			if (n <= _size)
			{
				_size = n;
				_str[_size] = '\0';
			}
			else
			{
				if (n > _capacity)
				{
					reserve(n);
				}
				for (size_t i = _size; i < n; i++)
				{
					_str[i] = ch;
				}
				_size = n;
				_str[_size] = '\0';
			}
		}
		//插入一个字符
		void push_back(char ch)
		{
			if (_size >= _capacity)
			{
				size_t newcapacity = _capacity == 0 ? 4 : _capacity * 2;
				reserve(newcapacity);
				//reserve(_capacity * 2);
			}
			_str[_size] = ch;
			_size++;
			_str[_size] = '\0';
		}
		//插入一个字符串
		void append(const char* str)
		{
			size_t len = strlen(str);//字符串长度
			if (_size + len > _capacity)
			{
				reserve(_size + len);
			}
			strcpy(_str + _size, str);
			_size += len;
		}

		string& operator+=(char ch)
		{
			push_back(ch);
			return *this;
		}
		string& operator+=(const char* str)
		{
			append(str);
			return *this;
		}
		string& operator+=(const string& s)
		{
			*this += s._str;
			return *this;
		}
		size_t size() const
		{
			return _size;
		}
		size_t capacity() const
		{
			return _capacity;
		}
		//在当前字符查找一个字符
		size_t find(char ch, size_t pos = 0)
		{
			for (size_t i = pos; i < _size; i++)
			{
				if (_str[i] == ch)
				{
					return i;
				}
			}
			return npos;//用npos表示没找到
		}
		//在当前字符查找一个字符串
		size_t find(const char* sub, size_t pos = 0)
		{
			const char* p = strstr(_str+pos,sub);
			if (p == nullptr)
			{
				return npos;//没找到
			}
			else
			{
				return p -_str;
			}
			return npos;
		}
		//在pos位置插入一个字符:
		string& insert(size_t pos, char ch)
		{
			assert(pos <= _size);
			if (_size == _capacity)
			{
				size_t newcapacity = _capacity == 0 ? 4 : _capacity * 2;
				reserve(newcapacity);
			}

			size_t end = _size + 1;
			while (end > pos)
			{
				_str[end] = _str[end - 1];
				end--;
			}
			/*以下这小段代码会发生整型提升
			int end = _size;
			while (end >= pos)
			{
				_str[end + 1] = _str[end];
				end--;
			}*/
			_str[pos] = ch;
			_size++;
			return *this;
		}
		//在pos位置插入一个字符串
		string& insert(size_t pos, const char* str)
		{
			assert(pos <= _size);
			size_t len = strlen(str);
			if (len == 0)
			{
				return *this;
			}
			if (len + _size > _capacity)
			{
				reserve(len + _size);
			}
			size_t end = _size + len;
			while (end >= pos+len)
			{
				_str[end] = _str[end - len];
				end--;
			}
			for (size_t i = 0; i < len; i++)
			{
				_str[pos + i] = str[i];
			}
			_size += len;
			return *this;
		}
		const char* c_str()
		{
			return _str;
		}
		//删除pos位置后面的字符
		string& erase(size_t pos, size_t len = npos)
		{
			assert(pos < _size);
			//pos后面删完
			if (len == npos || pos + len >= _size)
			{
				_str[pos] ='\0';
				_size = pos;
			}
			//pos后面删除部分
			else
			{
				strcpy(_str + pos, _str + pos + len);
				_size -= len;
			}
			return *this;
		}
		void clear()
		{
			_str[0] = '\0';
			_size = 0;
		}
	private:
		char* _str;
		size_t _size;
		size_t _capacity;//存多少有效数据的空间，不包含最后做标识的'\0'
		static const size_t npos;
	};

	const size_t string::npos = -1;

	//存在深拷贝对象，尽量少用
	string operator+(const string& s1, const string& str)
	{
		string ret = s1;
		ret += str;
		return ret;
	}

	//s1>s2——字符串比较
	bool operator>(const string& s1, const string& s2)
	{
		size_t i1 = 0;
		size_t i2 = 0;
		while (i1 < s1.size() && i2 < s2.size())
		{
			if (s1[i1] > s2[i2])
			{
				return true;
			}
			else if (s1[i1] < s2[i2])
			{
				return false;
			}
			else
			{
				return false;
			}
			i1++;
			i2++;
		}
		//注意！！！这里的情况
		if (i1 == s1.size())
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	//s1=s2——字符串比较
	bool operator==(const string& s1, const string& s2)
	{
		size_t i1;
		size_t i2;
		while (i1 < s1.size() && s2.size())
		{
			if (s1[i1] != s2[i2])
			{
				return false;
			}
			else
			{
				i1++;
				i2++;
			}
		}
		if (i1 == s1.size() && i2 == s2.size())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	//下面进行赋用其他的运算符
	inline bool operator!=(const string& s1, const string& s2)
	{
		return !(s1 == s2);
	}

	inline bool operator>=(const string& s1, const string& s2)
	{
		return s1 > s2 || s1 == s2;
	}

	inline bool operator<(const string& s1, const string& s2)
	{
		return !(s1 >= s2);
	}

	inline bool operator<=(const string& s1, const string& s2)
	{
		return !(s1 > s2);
	}
	//流输出
	ostream& operator<<(ostream& out, const string& s)
	{
		for (size_t i = 0; i < s.size(); i++)
		{
			out << s[i];
		}
		return out;
	}
	//流提取
	istream& operator>>(istream& in, string& s)
	{
		s.clear();
		char ch;
		//in >> ch;
		ch = in.get();
		while (ch != ' ' && ch !='\n')
		{
			s += ch;
			//in >> ch;
			ch = in.get();
		}
		return in;
	}
	void test_string1()
	{
		string s1("hello");
		s1[0] = 'x';

		string s2(s1);
		s2[0] = 'y';

		string s3("hello world");
		s1 = s3;

		s1.push_back(' ');
		s1.append("world");

		s1 += ' ';
		s1 += "world";
	}
	void test_string2()
	{
		string s1;
		string s2(s1);
		s1 += 'x';
		s1 += "hello";
		s1.resize(3);
		s1.resize(8);
		s1.resize(20);
	}
	//遍历 打印 方法一
	void print(const string& s)
	{
		for (size_t i = 0; i < s.size(); i++)
		{
			cout << s[i] << " ";
		}
		cout << endl;
		//迭代器的遍历
		string::const_iterator it = s.begin();
		while (it != s.end())
		{
			cout << *it << " ";
			it++;
		}
		cout << endl;
		//范围for
		//范围for的原理就是被替换成迭代器
		for (auto e : s)
		{
			cout << e << " ";
		}
		cout << endl;
	}
	//遍历 打印 方法二
	void test_string3()
	{
		string s1("hello world");
			for (size_t i = 0; i < s1.size(); i++)
			{
				cout << s1[i] << " ";
			}
			cout << endl;
			//迭代器的遍历
			string::const_iterator it = s1.begin();
			while (it != s1.end())
			{
				cout << *it << " ";
				it++;
			}
			cout << endl;
			//范围for
			//范围for的原理就是被替换成迭代器
			for (auto e : s1)
			{
				cout << e << " ";
			}
			cout << endl;
			print(s1);
	}
	void test_string4()
	{
		string s = "hello";
		s.insert(5,"hzs");
		s.erase(0,2);
		cout << s.c_str() << endl;
	}
	void test_string5()
	{
		//一般场景下，以下两种输出没有差别
		string s1("hello");
		cin >> s1;
		cout << s1 << endl;
		cout << s1.c_str() << endl;
		//但是这种场景有差别
		string s2("hello");
		s2.resize(20);
		s2[19] = 'x';
		cout << s2 << endl;
		//无法保证所以的都输出，按字符串识别——遇到'\0'终止
		cout << s2.c_str() << endl;

		string s3;
		cin >> s3;
		cout << s3 << endl;
	}
}
