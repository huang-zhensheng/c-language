#define _CRT_SECURE_NO_WARNINGS 1
using namespace std;
#include<iostream>
#include<vector>
#include<mutex>
//全局只要唯一的Singleton实例对象，那么他里面的成员也就是单例的
//饿汉模式:mian函数之前，一开始就创建对象
class Singleton
{
public:
	//第三步：提供一个单例的获取static成员函数
	static Singleton& GetInstance();//静态的不用对象去调，直接用类型去调用
private:
	//第一步：构造函数私有化，不能随意创建对象
	Singleton()
	{}
	//防拷贝
	Singleton(const Singleton&) = delete;
	Singleton& operator = (const Singleton&) = delete;
	//第二步：
	//类里面声明一个static Singleton对象，在cpp定义这个对象
	//保证全局只有一个唯一对象
	static Singleton* _sinst;//声明
	static mutex _mtx;
};

