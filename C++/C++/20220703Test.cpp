#define _CRT_SECURE_NO_WARNINGS 1
using namespace std;
#include<iostream>
#include<unordered_set>
#include<unordered_map>
#include<iostream>
void test_unordered_set()
{
	unordered_set<int> s;
	s.insert(1);
	s.insert(10);
	s.insert(3);
	s.insert(5);
	s.insert(30);
	unordered_set<int>::iterator it = s.begin();
	while (it != s.end())
	{
		cout << *it << " ";
	}
	cout << endl;
}
int  main()
{
	test_unordered_set();
	return 0;
}