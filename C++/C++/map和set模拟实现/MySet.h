#pragma once
#include"RBTree2.h"
namespace hzs
{
	template<class K>
	class set
	{
		struct SetkeyOfT
		{
			const K & operator()(const K& key)
			{
				return key;
			}
		};
	public:
		typedef typename RBTree<K, K, SetkeyOfT>/*��ģ��*/::iterator iterator;
		iterator begin()
		{
			return _t.begin();
		}
		iterator end()
		{
			return _t.end();
		}
		bool insert(const K& key)
		{
			return _t.Insert(key);
		}
	private:
		RBTree<K, K,SetkeyOfT> _t;
	};
	void test_set()
	{
		set<int> s;
		s.insert(1);
		s.insert(20);
		s.insert(3);
		s.insert(5);
		s.insert(2);

		set<int>::iterator it = s.begin();
		while (it != s.end())
		{
			cout << *it << " ";
			++it;
		}
		cout << endl;
	}
}