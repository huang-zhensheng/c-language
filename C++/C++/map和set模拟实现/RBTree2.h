#pragma once
#define _CRT_SECURE_NO_WARNINGS 1
using namespace std;
#include<iostream>
enum Colour
{
	BLACK,
	RED
};
template<class T>
struct RBTreeNode
{
	RBTreeNode<T>* _left;
	RBTreeNode<T>* _right;
	RBTreeNode<T>* _parent;

	Colour _col;
	T _data;

	RBTreeNode(const T& data)
		:_left(nullptr)
		, _right(nullptr)
		, _parent(nullptr)
		, _col(RED)
		, _data(data)
	{}
};
template<class T,class Ref,class Ptr>
struct RBTreeIterator
{
	typedef RBTreeNode<T> Node;
	typedef RBTreeIterator<T, Ref, Ptr> Self;
	Node* _node;
	RBTreeIterator(Node*node = nullptr)
		:_node(node)
	{}
	Ref operator*()
	{
		return _node->_data;
	}
	Ptr operator->()
	{
		return &_node->_data;
	}
	Self& operator++()
	{
		if (_node->_right)
		{
			//右子树中序第一个节点，即右子树的最左节点
			Node* subLeft = _node->_right;
			while (subLeft->_left)
			{
				subLeft = subLeft->_left;
			}
			_node = subLeft;
		}
		else
		{
			//当前子树已经访问完了，要去找祖先访问
			Node* cur = _node;
			Node* parent = cur->_parent;
			while (parent && parent->_right == cur)
			{
				cur = parent;
				parent = parent->_parent;
			}
			_node = parent;
		}
		return *this;
	}
	//比较两个迭代器是否相等
	bool operator!=(const Self& s) const
	{
		return _node != s._node;
	}
	bool operator ==(const Self& s) const
	{
		return _node == s._node;
	}
};
//如果T是k的话，set
//如果T是pair<const k,v>的话，map
template<class K, class T,class keyofT>
class RBTree
{
	typedef RBTreeNode<T>Node;
public:
	typedef RBTreeIterator<T, T&, T*> iterator;
	typedef RBTreeIterator<T, const T&,const T*> const_iterator;
	//找到最左节点
	iterator begin()
	{
		Node* left = _root;
		while (left && left->_left)
		{
			left = left->_left;
		}
		return left;
		//return iterator(left);
	}
	iterator end()
	{
		return iterator(nullptr);
	}
	RBTree()
		:_root(nullptr)
	{}

	pair<iterator, bool> Insert(const T& data)
	{
		if (_root == nullptr)
		{
			_root = new Node(data);
			_root->_col = BLACK;
			return make_pair(iterator(_root), true);
		}

		Node* parent = nullptr;
		Node* cur = _root;

		KeyOfT kot;
		while (cur)
		{
			if (kot(cur->_data) < kot(data))
			{
				parent = cur;
				cur = cur->_right;
			}
			else if (kot(cur->_data) > kot(data))
			{
				parent = cur;
				cur = cur->_left;
			}
			else
			{
				return make_pair(iterator(cur), false);
			}
		}

		// 新增节点，颜色是红色，可能破坏规则3，产生连续红色节点
		cur = new Node(data);
		Node* newnode = cur;
		cur->_col = RED;

		if (kot(parent->_data) < kot(data))
		{
			parent->_right = cur;
			cur->_parent = parent;
		}
		else
		{
			parent->_left = cur;
			cur->_parent = parent;
		}

		// 控制近似平衡
		while (parent && parent->_col == RED)
		{
			Node* grandfather = parent->_parent;
			if (parent == grandfather->_left)
			{
				Node* uncle = grandfather->_right;
				// 情况一：uncle存在且为红，进行变色处理，并继续往上更新处理
				if (uncle && uncle->_col == RED)
				{
					parent->_col = uncle->_col = BLACK;
					grandfather->_col = RED;

					cur = grandfather;
					parent = cur->_parent;
				} // 情况二+三：uncle不存在，或者存在且为黑，需要旋转+变色处理
				else
				{
					// 情况二：单旋+变色
					if (cur == parent->_left)
					{
						RotateR(grandfather);
						parent->_col = BLACK;
						grandfather->_col = RED;
					}
					else // 情况三：双旋 + 变色
					{
						RotateL(parent);
						RotateR(grandfather);
						cur->_col = BLACK;
						grandfather->_col = RED;
					}

					break;
				}
			}
			else  // (parent == grandfather->_right)
			{
				Node* uncle = grandfather->_left;
				if (uncle && uncle->_col == RED)
				{
					parent->_col = uncle->_col = BLACK;
					grandfather->_col = RED;

					cur = grandfather;
					parent = cur->_parent;
				}
				else
				{
					if (parent->_right == cur)
					{
						RotateL(grandfather);
						parent->_col = BLACK;
						grandfather->_col = RED;
					}
					else
					{
						RotateR(parent);
						RotateL(grandfather);
						cur->_col = BLACK;
						grandfather->_col = RED;
					}

					break;
				}
			}
		}

		_root->_col = BLACK;
		return make_pair(iterator(newnode), true);
	}
	//pair<iterator, bool> Insert(const T& data)
	//{
	//	if (_root == nullptr)
	//	{
	//		_root = new Node(data);
	//		_root->_col = BLACK;
	//		return make_pair(iterator(_root), true);
	//	}
	//	Node*parent = nullptr;
	//	Node*cur = _root;
	//	keyofT kot;
	//	while (cur)
	//	{
	//		if (kot(cur->_data) < kot(data))
	//		{
	//			parent = cur;
	//			cur = cur->_right;
	//		}
	//		else if (kot(cur->_data) > kot(data))
	//		{
	//			parent = cur;
	//			cur = cur->_left;
	//		}
	//		else
	//		{
	//			return make_pair(iterator(cur), false);
	//		}
	//	}
	//	//新增结点，颜色是红色的，可能会产生连续的红色结点，不符合红黑树的规则
	//	cur = new Node(data);
	//	cur->_col = RED;
	//	if (kot(parent->_data) < kot(data))
	//	{
	//		parent->_right = cur;
	//		cur->_parent = parent;
	//	}
	//	else
	//	{
	//		parent->_left = cur;
	//		cur->_parent = parent;
	//	}
	//	//控制近似平衡
	//	while (parent && parent->_col == RED)
	//	{
	//		Node* grandfather = parent->_parent;
	//		if (parent == grandfather->_left)
	//		{
	//			Node*uncle = grandfather->_right;
	//			//情况一:uncle存在且为红，进行变色处理即可,并且继续往上更新处理
	//			if (uncle && uncle->_col == RED)
	//			{
	//				parent->_col = uncle->_col = BLACK;
	//				grandfather->_col = RED;
	//				//继续往上处理
	//				cur = grandfather;
	//				parent = cur->_parent;
	//			}
	//			//情况二+三:uncle不存在，或者存在且为黑，需要旋转+变色处理
	//			else
	//			{
	//				//情况二:单旋+变色
	//				if (cur == parent->_left)
	//				{
	//					RotateR(grandfather);
	//					parent->_col = BLACK;
	//					grandfather->_col = RED;
	//				}
	//				//情况三:双旋+变色
	//				else
	//				{
	//					RotateL(parent);
	//					RotateR(grandfather);
	//					cur->_col = BLACK;
	//					grandfather->_col = RED;
	//				}
	//				break;
	//			}
	//		}
	//		else //parent == grandfather->_right
	//		{
	//			Node*uncle = grandfather->_left;
	//			//情况一:uncle存在且为红，进行变色处理即可,并且继续往上更新处理
	//			if (uncle && uncle->_col == RED)
	//			{
	//				parent->_col = uncle->_col = BLACK;
	//				grandfather->_col = RED;
	//				//继续往上处理
	//				cur = grandfather;
	//				parent = cur->_parent;
	//			}
	//			//情况二+三:uncle不存在，或者存在且为黑，需要旋转+变色处理
	//			else
	//			{
	//				//情况二:单旋+变色
	//				if (parent->_right == cur)
	//				{
	//					RotateL(grandfather);
	//					parent->_col = BLACK;
	//					grandfather->_col = RED;
	//				}
	//				//情况三:双旋+变色
	//				else
	//				{
	//					RotateR(parent);
	//					RotateL(grandfather);
	//					cur->_col = BLACK;
	//					grandfather->_col = RED;
	//				}
	//				break;
	//			}
	//		}
	//	}
	//	_root->_col = BLACK;
	//	return make_pair(iterator(newnode), true);
	//}
	void RotateR(Node* parent)//右旋
	{
		Node* subL = parent->_left;
		Node* subLR = subL->_right;

		parent->_left = subLR;
		if (subLR)
			subLR->_parent = parent;

		Node*ppNode = parent->_parent;

		subL->_right = parent;
		parent->_parent = subL;

		if (parent == _root)
		{
			_root = subL;
			subL->_parent = nullptr;
		}
		else
		{
			if (ppNode->_left == parent)
				ppNode->_left = subL;
			else
				ppNode->_right = subL;

			subL->_parent = ppNode;
		}
	}
	void RotateL(Node* parent)//左旋
	{
		Node* subR = parent->_right;
		Node* subRL = subR->_left;

		parent->_right = subRL;

		if (subRL)
		{
			subRL->_parent = parent;
		}
		Node* ppNode = parent->_parent;

		subR->_left = parent;
		parent->_parent = subR;

		if (parent == _root)
		{
			_root = subR;
			subR->_parent = nullptr;
		}
		else
		{
			if (ppNode->_left == parent)
				ppNode->_left = subR;
			else
				ppNode->_right = subR;

			subR->_parent = ppNode;
		}
	}
	void _InOrder(Node* root)
	{
		if (root == nullptr)
		{
			return;
		}
		else
		{
			_InOrder(root->_left);
			cout << root->_kv.first << " ";
			_InOrder(root->_right);
		}
	}
	void InOrder()
	{
		_InOrder(_root);
		cout << endl;
	}
	bool Check(Node* root)
	{
		if (root == nullptr)
		{
			return true;
		}
		if (root->_col == RED && root->_parent->_col == RED)
		{
			cout << "存在连续的红色节点" << endl;
			return false;
		}
		return Check(root->_left)
			&& Check(root->_right);
	}
	//检查每条路径黑色结点的数目
	bool Check_BLACK(Node* root, int blackNum, int benchmark)
	{
		if (root == nullptr)
		{
			if (blackNum != benchmark)
			{
				cout << "黑色结点的数目不相等" << endl;
				return false;
			}
			return true;
		}
		if (root->_col == BLACK)
		{
			blackNum++;
		}
		return Check_BLACK(root->_left, blackNum, benchmark)
			&& Check_BLACK(root->_right, blackNum, benchmark);
	}
	bool IsBalance()
	{
		if (_root == nullptr)
		{
			return true;
		}
		if (_root->_col == RED)
		{
			cout << "根节点是红色" << endl;
			return false;
		}
		//算出最左路径的黑色结点的数量作为基准值
		int benchmark = 0;
		Node* cur = _root;
		while (cur)
		{
			if (cur->_col == BLACK)
			{
				benchmark++;
			}
			cur = cur->_left;
		}
		int blackNum = 0;
		return Check(_root) && Check_BLACK(_root, blackNum, benchmark);
	}
private:
	Node* _root;
};
//void TestRBTree()
//{
//	int a[] = { 16, 3, 7, 11, 9, 26, 18, 14, 15, 34, 100, 56 };
//	RBTree<int, int>t1;
//	for (auto e : a)
//	{
//		t1.Insert(make_pair(e, e));
//	}
//	cout << t1.IsBalance() << endl;
//	t1.InOrder();
//}