#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include <deque>
#include <iostream>
#include <queue>
using namespace std;
namespace hzs
{
	template<class T,class Container = std::deque<T>>
	class queue
	{
		//Container尾认为是队尾，头认为是队头
	public:
		void push(const T& x)
		{
			_con.push_back(x);
		}
		void pop()
		{
			_con.pop_front();
		}
		const T& front()
	   	{
			return _con.front();
		}
		const T& back()
		{
			return _con.back();
		}
		size_t size()
		{
			return _con, size();
		}
		bool  empty()
		{
			return _con.empty();
		}
	private:
		Container _con;
	};
	void test_queue()
	{
		queue<int>st;
		st.push(1);
		st.push(2);
		st.push(3);
		st.push(4);
		while (!st.empty())
		{
			cout << st.front() << " ";
			st.pop();
		}
		cout << endl;
	}
}