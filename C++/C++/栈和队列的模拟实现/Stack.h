#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include <deque>
#include <iostream>
#include <stack>
using namespace std;
namespace hzs
{
	// stack 是一个Container 适配(封装转换)出来的
	template<class T, class Container = std::deque<T>>
	class stack
	{
		//Container 尾认为是栈顶
	public:
		void push(const T& x)
		{
			_con.push_back(x);
		}

		void pop()
		{
			_con.pop_back();
		}

		const T& top()
		{
			return _con.back();
		}

		size_t size()
		{
			return _con.size();
		}

		bool  empty()
		{
			return _con.empty();
		}

	private:
		Container _con;
	};
	void test_stack()
	{
		stack<int>st;
		st.push(1);
		st.push(2);
		st.push(3);
		st.push(4);
		while (!st.empty())
		{
			cout << st.top() << " ";
			st.pop();
		}
		cout << endl;
	}
}