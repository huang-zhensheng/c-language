#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<list>
#include<vector>
#include<algorithm>
#include<functional>
#include<time.h>
#include<assert.h>
using namespace std;
#pragma once
namespace hzs
{
	template<class T>
	struct __list_node//内部使用
	{
		__list_node<T>* _next;
		__list_node<T>* _prev;
		T _data;

		__list_node(const T& x = T())
			:_next(nullptr)
			, _prev(nullptr)
			, _data(x)
		{}
	};
	// 迭代器(自定义类型)-- 用一个类去封装了节点的指针
	template<class T, class Ref, class Ptr>
	struct __list_iterator
	{
		typedef __list_iterator<T, Ref, Ptr> self;
		typedef __list_node<T> Node;
		Node* _node;//包含节点的指针

		__list_iterator(Node* node)
			:_node(node)//节点的指针初始化
		{}

		// *it->it.operator*()
		/*T& operator*()
		{
		return _node->_data;
		}*/
		Ref operator*()
		{
			return _node->_data;
		}
		Ptr operator->()
		{
			return &_node->_data;
		}
		// 前置++it
		self& operator++()
		{
			_node = _node->_next;

			return *this;
		}
		//后置it++  ->it.operator(0)
		self operator++(int)//注意不能用引用返回
		{
			self tmp(*this);
			_node = _node->_next;
			return tmp;
		}
		//判断两个迭代器是否相等 —> 判断是否指向的是同一个节点
		bool operator!= (const self& it) const
		{
			return _node != it._node;
		}
	};

	template<class T>
	class list
	{
		typedef __list_node<T>  Node;
	public:
		typedef __list_iterator<T, T&, T*>  iterator;
		typedef __list_iterator<T, const T&, const T*>  const_iterator;

		//typedef __list_iterator<T>  const_iterator;
		iterator begin()
		{
			return iterator(_head->_next);//第一个位置的节点指针
		}

		iterator end()
		{
			return iterator(_head);//最后一个数据的下一个位置
		}
		const_iterator begin() const
		{
			return const_iterator(_head->_next);//第一个位置的节点指针
		}

		const_iterator end() const
		{
			return const_iterator(_head);//最后一个数据的下一个位置
		}
		list()
		{
			// 带头双向循环
			//_head = new Node(T());
			_head = new Node;//头结点
			_head->_next = _head;
			_head->_prev = _head;
		}
		//析构函数
		~list()
		{
			clear();
			delete _head;
			_head = nullptr;
		}
  //  	//lt2(lt1)-----传统写法
		//list(const list<T>& lt)
		//{
		//	//哨兵位的头结点
		//	_head = new Node;
		//	_head->_next = _head;
		//	_head->_prev = _head;
		//	for (const auto& e : lt)
		//	{
		//		push_back(e);
		//	}
		//}
		//现代写法
		template<class InputIterator>
		list(InputIterator first, InputIterator last)
		{ 
			_head = new Node;
			_head->_next = _head;
			_head->_prev = _head;
			while (first != last)
			{
				push_back(*first);
				first++;
			}
		}
		//lt2(lt1)-----现代写法
		list(const list<T>& lt)
		{
			_head = new Node;
			_head->_next = _head;
			_head->_prev = _head;

			list<T>tmp(lt.begin(), lt.end());
			std::swap(_head, tmp._head);
		}


		////lt1 = lt4 -----传统写法
		//list<T>& operator = (const list<T>& lt)
		//{
		//	if (this != &lt)
		//	{
		//		clear();
		//		for (const auto& e : lt)
		//		{
		//			push_back(e);
		//		}
		//	}
		//	return *this;
		//}
		//现代写法
		//lt1 = lt4
		list<T>operator = (list<T> lt)
		{
			swap(_head, lt._head);
			return *this;
		}

		//void clear()
		//{
		//	iterator it = begin();
		//	while (it != end())
		//	{
		//		Node* del = it->_node;//保存下一个位置
		//		it++;
		//		delete del;
		//	}
		//	_head->_next = _head;
		//}
		void clear()
		{
			iterator it = begin();
			while (it != end())
			{
				it = erase(it);
			}
		}
		void push_back(const T& x)
		{
			Node* tail = _head->_prev;
			Node* newnode = new Node(x);
			//尾插
			tail->_next = newnode;
			newnode->_prev = tail;
			newnode->_next = _head;
			_head->_prev = newnode;
		}
		iterator insert(iterator pos, const T& x)
		{
			Node* cur = pos._node;
			Node* prev = cur->_prev;

			Node*newnode = new Node(x);
			prev->_next = newnode;
			newnode->_prev = prev;
			newnode->_next = cur;
			cur->_prev = newnode;

			return  iterator(newnode);

			//或者这样写
			//iterator ret(newnode);
			//return ret;
		}
		//删除
		iterator erase(iterator pos)
		{
			assert(pos != end());//不能删头结点

			Node*cur = pos._node;
			Node*prev = cur->_prev;
			Node*next = cur->_next;

			delete cur;
			prev->_next = next;
			next->_prev = prev;

			return iterator(next);
		}
		//删尾
		void pop_back()
		{
			erase(end()--);
		}
		//删头
		void pop_front()
		{
			erase(begin());
		}
		size_t size()
		{
			size_t n = 0;
			iterator it = begin();
			while (it != end())
			{
				it++;
				n++;
			}
			return n;
		}
		bool empty()
		{
			return begin() == end();
		}
	private:
		Node* _head;
	};
	void print_list(const list<int>& l)
	{
		list<int>::const_iterator it = l.begin();
		while (it != l.end())
		{
			cout << *it << " ";
			it++;
		}
		cout << endl;
	}
	struct AA
	{
		struct AA* _left;
		struct AA* _right;
		int _val;

		AA(int val=1)
			:_left(nullptr)
			, _right(nullptr)
			, _val(val)
		{}
	};
	void test_list3()
	{
		list<int>lt1;
		lt1.push_back(1);
		lt1.push_back(2);
		lt1.push_back(3);
		lt1.push_back(4);
		for (auto e : lt1)
		{
			cout << e << " ";
		}
		cout << endl;
		//拷贝
		list<int>lt2(lt1);
		for (auto e : lt2)
		{
			cout << e << " ";
		}
		cout << endl;

		list<int>lt3(lt1.begin(), lt1.end());
		string s("hello");
		list<int>lt4(s.begin(), s.end());
		for (auto e : lt3)
		{
			cout << e << " ";
		}
		cout << endl;
		for (auto e : lt4)
		{
			cout << e << " ";
		}
		cout << endl;
		//赋值
		lt1 = lt4;
		for (auto e : lt1)
		{
			cout << e << " ";
		}
		cout << endl;
	}

	void test_list2()
	{
		list<AA>lt;
		lt.push_back(AA(1));
		lt.push_back(AA(2));
		lt.push_back(AA(3));
		lt.push_back(AA(4));

		list<AA>::iterator it = lt.begin();
		while (it != lt.end())
		{
			printf("val:%d,left:%p,right:%p\n", (*it)._val, (*it)._left, (*it)._right);
			printf("val:%d,left:%p,right:%p\n", it->_val, it->_left, it->_right);
			it++;
		}
		cout << endl;
	}
	void test_list1()
	{
		list<int> lt;
		lt.push_back(1);
		lt.push_back(2);
		lt.push_back(3);
		lt.push_back(4);
		print_list(lt);
		lt.clear();
		lt.push_back(10);
		lt.push_back(20);
		lt.push_back(30);
		lt.push_back(40);

		for (auto e : lt)
		{
			cout << e << " ";
		}
		cout << endl;

		list<int>::iterator it = lt.begin();
		while (it != lt.end())
		{
			//it.operator*() = 10;

			*it = 10;
			cout << *it << " ";
			++it;
		}
		cout << endl;

		for (auto e : lt)
		{
			cout << e << " ";
		}
		cout << endl;
	}
}