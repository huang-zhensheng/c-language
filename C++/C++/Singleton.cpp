#define _CRT_SECURE_NO_WARNINGS 1
#include"Singleton.h"
Singleton* Singleton::_sinst;//����
mutex Singleton::_mtx;

Singleton& Singleton::GetInstance()
{
	//˫������
	if (_sinst == nullptr)
	{
		_mtx.lock();
		if (_sinst == nullptr)
		{
			_sinst = new Singleton;
		}
		_mtx.unlock();
	}
	return *_sinst;
}

