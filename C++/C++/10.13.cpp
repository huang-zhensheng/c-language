#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<assert.h>
using namespace std;
class Date
{
public:
	//获取某年某月的天数
	int GetMonthDay(int year, int month)
	{
		assert(month > 0 || month < 13);
		static int monthDays[13] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
		//闰年2月是29天
		if (month == 2 && ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0))
		{
			return 29;
		}
		return monthDays[month];
	}
	Date(int year = 0, int month = 1, int day = 1)
	{
		_year = year;
		_month = month;
		_day = day;
		//判断日期是否合法
		if (_year < 0 
			|| _month <= 0 || _month >= 13 
			|| _day <=0 || _day > GetMonthDay(_year,_month))
		{
			cout << _year << "/" << _month << "/" << _day << "—>" << "非法日期" << endl;
		}
	}
	void print()
	{
		cout << _year << "/" << _month << "/" << _day << endl;
	}
	//d1+100
	Date operator+(int day)
	{
		Date ret = *this;
		ret += day;
		return ret;
	}
	//d1-=100
	Date& operator-=(int day)
	{
		if (day < 0)
		{
			return *this += -day;
		}
		_day -= day;
		while (_day <=0)
		{
			--_month;
			if (_month == 0)
			{
				--_year;
				_month = 12;
			}
			_day += GetMonthDay(_year, _month);
		}
		return *this;
	}
	//d1+=100
	Date& operator+=(int day)
	{
		if (day < 0)
		{
			return *this -= -day;
		}
		_day += day;
		while (_day > GetMonthDay(_year, _month))
		{
			_day -= GetMonthDay(_year, _month);
			++_month;
			if (_month == 13)
			{
				++_year;
				_month = 1;
			}
		}
		return *this;
	}
private:
	int _year;
	int _month;
	int _day;
};
int main()
{
	Date d1(2021, 3, 29);
	Date d2 = d1 + -100;
	d2.print();
	return 0;
}

//bool operator < (const Date& x)
//{
//	if (_year < x._year)
//		return true;
//	else if (_year == x._year && _month < x._month)
//		return true;
//	else if (_year == x._year && _month == x._month && _day < x._day)
//		return true;
//	else
//		return false;
//}

//class Date
//{
//public:
//	Date(int year = 0, int month = 1, int day = 1)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//	bool operator < (const Date& x)
//	{
//		if (_year < x._year)
//			return true;
//		else if (_year == x._year && _month < x._month)
//			return true;
//		else if (_year == x._year && _month == x._month && _day < x._day)
//			return true;
//		else
//			return false;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//int main()
//{
//	Date d1(2021, 11, 11);
//	Date d2(2020, 11, 11);
//	Date d3(2021, 11, 11);
//	d1 < d2;
//	cout << (d2 < d3) << endl;
//	cout << (d1 < d2) << endl;
//	return 0;
//}

//class Date
//{
//public:
//	Date(int year=0, int month=1, int day=1)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//	//运算符重载：
//	//d1==d2 —>d1.operator == (d2)—>d1.operator ==(&d1,d2)
//	//bool operator == (Date*this,const Date& x)//d1传给了this,d2传给了x
//	bool operator == (const Date& x)
//	{
//		return this->_year == x._year
//			&& this->_month == x._month
//			&& this->_day == x._day;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//int main()
//{
//	Date d1(2021, 11, 11);
//	Date d2(2020, 11, 11);
//	Date d3(2021, 11, 11);
//	d1.operator == (d2);
//	d1 == d2;//转换成operator ==(d1,d2)
//	return 0;
//}
//class Time
//{
//public:
//	Time()
//	{
//		cout << "Time()" << endl;
//		_hour = 0;
//		_minute = 0;
//		_second = 0;
//	}
//private:
//	int _hour;
//	int _minute;
//	int _second;
//};
//class Date
//{
//private:
//	// 基本类型(内置类型)
//	int _year;
//	int _month;
//	int _day;
//	// 自定义类型
//	Time _t;
//};
//int main()
//{
//	Date d;
//	return 0;
//}

//class Stack
//{
//public:
//	Stack(int capacity = 4)
//	{
//		_a = (int*)malloc(sizeof(int)*capacity);
//		if (_a == nullptr)
//		{
//			cout << "malloc fail" << endl;
//			exit(-1);
//		}
//		_top = 0;
//		_capacity = capacity;
//	}
//	~Stack()
//	{
//		free(_a);
//		_a = nullptr;
//		_top = _capacity = 0;
//	}
//private:
//	int* _a;
//	int _top;
//	int _capacity;
//};
//int main()
//{
//	Stack st1;
//	Stack st2(st1);
//	return 0;
//}

//class Date
//{
//public:
//	Date(int year, int month, int day)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//	Date(const Date& d)
//	{
//		_year = d._year;
//		_month = d._month;
//		_day = d._day;
//	}
//	void Print()
//	{
//		cout << _year << "/" << _month << "/" << _day << endl;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//int main()
//{
//	Date d1(2021, 10, 9);
//	Date d2(d1);
//	d2.Print();
//	return 0;
//}