#include<iostream>
#include<vector>
using namespace std;
#include"vector.h"
int main()
{
	iterator insert(iterator pos, const T& x)
	{
		assert(pos >= _start && pos <= _finish);
		size_t len = pos - _start;
		if (_finish == _endofstroage)
		{
			size_t newcapacity = capacity() == 0 ? 4 : capacity() * 2;
			reserve(newcapacity);  
			pos = _start + len;
		}
		iterator end = _finish - 1;
		while (end >= pos)
		{
			*(end + 1) = *end;
			end -- ;
		}
		*pos = x;
		_finish++;
		return pos;
	}
	//hzs::test_vector1();
	return 0;
}