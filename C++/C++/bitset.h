#define _CRT_SECURE_NO_WARNINGS 1
#pragma once

namespace bit
{
	template<size_t N>
	class bitset
	{
	public:
		bitset()
		{ 
			_bits.resize(N / 8 + 1, 0);
		}
		bool test(size_t x)
		{
			// 计算出他是第i个char对象中
			size_t i = x / 8;
			// 计算出他在这个char的第j个比特位上
			size_t j = x % 8;

			//return _bits[i] & (1 << j);
			return ((_bits[i] & (1 << j)) == 0) ? false : true;
		}

		// 把x映射的比特位设置成1
		void set(size_t x)
		{
			// 计算出他是第i个char对象中
			size_t i = x / 8;
			// 计算出他在这个char的第j个比特位上
			size_t j = x % 8;

			_bits[i] |= (1 << j);
		}
		// 把x映射的比特位设置成0
		void reset(size_t x)
		{
			// 计算出他是第i个char对象中
			size_t i = x / 8;
			// 计算出他在这个char的第j个比特位上
			size_t j = x % 8;

			_bits[i] &= (~(1 << j));
		}

	private:
		vector<char> _bits;
	};

	/*void TestBitSet()
	{
	bitset<100> bs;
	bs.set(4);
	cout << bs.test(4) << endl;
	bs.reset(4);
	cout << bs.test(4) << endl;

	bs.set(50);
	cout << bs.test(50) << endl;
	bs.reset(50);
	cout << bs.test(50) << endl;
	}*/

	void TestBitSet()
	{
		//bitset<-1> bs;
		bitset<0xffffffff> bs;
	}

	template<size_t N>
	class FindOnceValSet
	{
	public:
		void set(size_t x)
		{
			bool flag1 = _bs1.test(x);
			bool flag2 = _bs2.test(x);
			// 00 -> 01
			if (flag1 == false && flag2 == false)
			{
				_bs2.set(x);
			}// 01 -> 10
			else if (flag1 == false && flag2 == true)
			{
				_bs1.set(x);
				_bs2.reset(x);
			}
			// 10 -> 10  不处理，标识已经出现多次
		}

		void print_once_num()
		{
			for (size_t i = 0; i < N; ++i)
			{
				if (_bs1.test(i) == false && _bs2.test(i) == true)
				{
					cout << i << endl;
				}
			}
		}
	private:
		bitset<N> _bs1;
		bitset<N> _bs2;
	};

	void TestFindOnceValSet()
	{
		int a[] = { 1, 20, 30, 43, 5, 4, 1, 43, 43, 7, 9, 7, 7, 0 };
		FindOnceValSet<100> fovs;
		for (auto e : a)
		{
			fovs.set(e);
		}

		fovs.print_once_num();
	}
}