#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<list>
#pragma once
using namespace std;
class Solution
{
public:
	bool IsPopOrder(vector<int>pushV, vector<int>popV)
	{
		stack<int>st;
		size_t pushi = 0, popi = 0;
		while (pushi < pushV.size())
		{
			st.push(pushV[pushi]);
			pushi++;
		}

		while (!st.empty() && st.top() == popV[popi])
		{
			popi++;
			st.pop();
		}
		return st.empty();
	}
};

//namespace hzs
//{
//	template<class T>
//	struct __list_node
//	{
//		__list_node<T>* _next;
//		__list_node<T>* _prev;
//		T _data;
//		 
//		__list_node(const T& x = T())
//			:_next(nullptr)
//			, _prev(nullptr)
//			, _data(x)
//		{}
//	};
//	template<class T>
//	struct __list_iterator
//	{
//		typedef __list_node<T>Node;
//		Node* _node;
//
//		__list_iterator(Node* node)
//			:_node(node)
//		{}
//
//		T& operator*()
//		{
//			return _node->_data;
//		}
//		//++it
//		__list_iterator<T>& operator++()
//		{
//			_node = _node -> _next;
//			return *this;
//		}
//		bool operator!=(const __list_iterator<T>& it)
//		{
//			return _node != it._node;
//		}
//	};
//	template<class T>
//	class list
//	{
//		typedef __list_node<T> Node;
//	public:
//		typedef __list_iterator<T> iterator;
//		iterator begin()
//		{
//			return iterator(_head->_next);
//		}
//		iterator end()
//		{
//			return iterator(_head);
//		}
//		list()
//		{
//			_head = new Node(T());
//			_head->_next = _head;
//			_head->_prev = _head;
//		}
//		void push_back(const T&x)
//		{
//			Node* tail = _head->_prev;
//			Node* newnode = new Node(x);
//			//β��
//			tail->_next = newnode;
//			newnode->_prev = tail;
//			newnode->_next = _head;
//			_head->_prev = newnode;
//		}
//	private:
//		Node* _head;
//	};
//	void test_list1()
//	{
//		list<int>lt;
//		lt.push_back(1);
//		lt.push_back(2);
//		lt.push_back(3);
//
//		list<int>::iterator it = lt.begin();
//		while (it != lt.end())
//		{
//			*it = 10;
//			cout << *it << " ";
//			++it;
//		}
//		cout << endl;
//	}
//}