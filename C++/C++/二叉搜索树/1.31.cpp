#define _CRT_SECURE_NO_WARNINGS 1
#include"BinarySearchTree.h"
void TestBSTree1()
{
	int a[] = { 5, 3, 4, 1, 7, 8, 2, 6, 0, 9 };
	BSTree<int> t;
	for (auto e : a)
	{
		t.Insert(e);
	}
	t.InOrder();

	t.Erase(7);
	t.InOrder();

	t.Erase(5);
	t.InOrder();
	for (auto e : a)
	{
		t.Erase(e);
		t.InOrder();
	}
}

int main()
{
	TestBSTree1();

	return 0;
}